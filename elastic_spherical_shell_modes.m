% -----------------------------------------------------------------------
function [kk, ff, BB] = elastic_spherical_shell_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max)
% -----------------------------------------------------------------------
% computes the 'k' kk, frequency ff and B coefft of acoustic modes (nn,ll)
% in a spherical shell of fluid
% enclosed between an inner solid shell and an outer solid shell.
% Both the inner and the outer shells can be either rigid or elastic
% Seismological notation used (ex: 0S13, 1S0, etc)
% note : ff = kk * csound/(2 pi r_o)
% 140124 : adapted from paper by Rand and DiMaggio (1967), following Aurélien modes_sphere matlab program
% the pressure in the fluid is expressed as :
%	P(r,cos(theta)) = P_l(cos(theta)) * [j_l(kr) + B y_l(kr)]
% where j_l and y_l are the spherical Bessel functions of the first and second kind
% we define the 'j_term' and 'y_term' at the boundaries to get the frequency equation
% 
% inputs :
%	csound = sound velocity (m/s)
%	rho_f = density of the fluid (kgm^-3)
%	rf(1) = radius of the inner sphere (0 if none) (m)
%	rf(2) = radius of the outer sphere (m)
%	hat(1) = thickness of the inner elastic shell (m)
%	hat(2) = thickness of the outer elastic shell (m)
%	mu(1) = shear modulus of the inner elastic shell (Pa) (Inf if rigid)
%	mu(2) = shear modulus of the outer elastic shell (Pa) (Inf if rigid)
%	nu(1) = Poisson's ratio of the inner elastic shell
%	nu(2) = Poisson's ratio of the outer elastic shell
%	rho(1) = density of the inner elastic shell (kgm^-3)
%	rho(2) = density of the outer elastic shell (kgm^-3)
%	n_max = max radial mode number (0 to n_max)
%	l_max = max angular mode number (0 to l_max)
%
% outputs :
%	kk(nn+1,ll+1) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	ff(nn+1,ll+1) = frequency of mode nnS_ll (Hz)
%	BB(nn+1,ll+1) = factor of the y_l part for mode nnS_ll
%
disp(['Welcome in elastic_spherical_shell_modes,' char(10) 'which computes the acoustic modes in a spherical shell of fluid' char(10) 'enclosed between a solid inner shell and a solid outer shell.' char(10) 'Shells are either rigid or elastic.'])	    
% derived relevant parameters
lambda = rho * csound^2 ./ mu / 2.; % lambda(1:2) parameter of Rand and DiMaggio
kappa_0 = (rho_f./rho) .* (rf./hat); % kappa_0(1:2) parameter of Rand and DiMaggio

if (rf(1) == 0) % no inner sphere
% full fluid sphere with rigid or elastic shell
	f_sphere = @(l,x) j_term(l,x,lambda(2),kappa_0(2),nu(2));
%
	kk = f_modes(f_sphere, n_max, l_max); % 'k' of modes (0:n_max,0:l_max)
	BB = zeros(n_max+1,l_max+1); % no y_l term
%
else % there is an inner sphere
	f_sphere = @(l,x) (j_term(l,x,lambda(2),kappa_0(2),nu(2)) * y_term(l,x*rf(1)/rf(2),lambda(1),kappa_0(1),nu(1)) - y_term(l,x,lambda(2),kappa_0(2),nu(2)) * j_term(l,x*rf(1)/rf(2),lambda(1),kappa_0(1),nu(1)));
%
	kk = f_modes(f_sphere, n_max, l_max); % 'k' of modes (0:n_max,0:l_max)
%
	for ll=0:l_max
		l = ll+1;
		clear z
		z(1:n_max+1) = kk(1:n_max+1,l); % the computed k (or z) of all modes with ll angular number
%
% Amplitude BB of the y_l term (relative to the j_l term) from outer boundary condition
		BB(1:n_max+1,l) = -j_term(ll,z(1:n_max+1),lambda(2),kappa_0(2),nu(2))./y_term(ll,z(1:n_max+1),lambda(2),kappa_0(2),nu(2));
	end
end
%
ff = kk*csound/(2*pi*rf(2)); % frequency of modes (nn,ll)
%
end % end function

% ---------------------------------
function z = f_modes(f, nmax, lmax)
% ---------------------------------
	z = zeros(nmax+1,lmax+1);	
	for l = 0:lmax
		z(:,l+1) = fzeros(@(x) f(l,x), l+1, Inf, nmax+1, 1);
	end	    
% correction pour le mode (0,0)
	z(:,1)=[0; z(1:end-1,1)];
end

% -------------------------------------------
function z = fzeros(f, x0, xmax, nmax, delta)
% -------------------------------------------
%% fzeros : résolution de f(x) = 0
% f = fonction dont on cherche les zéros
% x0 = point de départ (début intervalle : doit être inférieur à tous les
% zéros recherchés)
% xmax = fin de l'intervalle de recherche (peut être +Inf)
% nmax = nombre de zéros recherchés (peut être +Inf)
% delta = écart minimal entre les zéros
%
	threshold = 1;   % limite déterminant si deux valeurs trouvées sont égales
	alloc_step = 128;   % si le nombre de zéros n'est pas spécifié, on alloue un tableau par tranches arbitraires
    
	if nmax < Inf
		z = zeros(nmax,1);
	else
		z = zeros(alloc_step,1);
	end

	n = 1;
	x = x0;
	while x < xmax && n <= nmax
% recherche le zéro le plus proche
%        if octave
%            [xn, ~, err] = fsolve(f, x); % 130829 HCN : replace fzero by fsolve
%        else
    		[xn, ~, err] = fzero(f, x, optimset('Display','off'));
%        end
% vérifie si on a trouvé une nouvelle valeur
	if err == 1 && (n == 1 || abs(xn - z(n - 1)) > threshold)
		z(n) = xn;
		n = n + 1;
% augmente la taille du tableau si nécessaire
			if nmax == Inf && mod(n, alloc_step) == 0
				z = [z ; zeros(alloc_step, 1)];
			end
			x = xn;
		end
		x = x + delta;
	end
	z(n:end) = [];
end

% ================== fonctions for elastic shell =================== %
% -----------------------------------------
function f = j_term(ll,z,lambda,kappa_0,nu)
% -----------------------------------------
% elastic case from equation (18) of Rand and DiMaggio (1967)
% with g123 combining the g1, g2 and g3 given in their Appendix (eqns A25-A27)
% and g456 combining the g4, g5 and g6 given in their Appendix (eqns A28-A30)
% NB: z can be a vector
%
	if (lambda == 0) % rigid shell
		f = d_spherical_bessel_j(ll,z);
	else % elastic shell
		g123 = (1 + lambda*z.^2) .* (2 - (1-nu)/(1+nu)*lambda*z.^2) - ll*(ll+1)*(1 - lambda*z.^2/(1+nu));
		g456 = lambda*kappa_0*z./(1+nu) .* (ll*(ll+1) - (1-nu)*(1+lambda*z.^2));
		f = g123 .* d_spherical_bessel_j(ll,z) + g456 .* spherical_bessel_j(ll,z);
	end
end
% -----------------------------------------
function f = y_term(ll,z,lambda,kappa_0,nu)
% -----------------------------------------
% elastic case from equation (18) of Rand and DiMaggio (1967)
% with g123 combining the g1, g2 and g3 given in their Appendix (eqns A25-A27)
% and g456 combining the g4, g5 and g6 given in their Appendix (eqns A28-A30)
% The y_term is added to deal with an inner sphere
% NB: z can be a vector
%
	if (lambda == 0) % rigid shell
		f = d_spherical_bessel_y(ll,z);
	else
		g123 = (1 + lambda*z.^2) .* (2 - (1-nu)/(1+nu)*lambda*z.^2) - ll*(ll+1)*(1 - lambda*z.^2/(1+nu));
		g456 = lambda*kappa_0*z./(1+nu) .* (ll*(ll+1) - (1-nu)*(1+lambda*z.^2));
		f = g123 .* d_spherical_bessel_y(ll,z) + g456 .* spherical_bessel_y(ll,z);
	end
end
% 
% NB: spherical Bessel functions and derivatives in acoustic_library folder
% =========================================================================