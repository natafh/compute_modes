% -----------------------------------------------------------
% Program to re-order the acoustic modes of the elastic case
% -----------------------------------------------------------
% 140124 HCN
%
epsilon = 1.e-1; % *** à tester ***
%
filename = input(['Enter the name of the modes file to treat :' char(10)],'s');
load(filename)
disp([filename ' loaded'])
%
disp(['ReadMe message for this file :' char(10) ReadMe_elastic_modes]); % the readme message of that file
%
if (parameters.mu(1) == Inf && parameters.mu(2) == Inf) % all rigid ->  no need to re-order
	disp(['Case of rigid boundaries : no need to re-order !'])
	stop
else % at least one boundary was elastic -> compute modes for the corresponding rigid case
% prepare new matrices for re-ordered modes
%	matrix_size = size(kk);
%	kk_new = zeros(matrix_size);
%	ff_new = zeros(matrix_size);
%	BB_new = zeros(matrix_size);
%	Acc_new = zeros(matrix_size);
%
	disp(['Computing equivalent case with rigid boundaries...'])
	mu(1) = Inf;
	mu(2) = Inf;
	csound = parameters.csound;
	rho_f = parameters.rho_f;
	rf = parameters.rf;
	hat = parameters.hat;
	nu = parameters.nu;
	rho = parameters.rho;
% same n_max and l_max
%
	[kk_r] = elastic_spherical_shell_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max);
%
% look for correspondances
	for ll = 0:l_max
		l = ll+1;
		ispec = 0;
		for nn = 0:n_max
			n = nn+1;
			ind = find(abs(kk_r(:,l) - kk(n,l)) <= epsilon); % looks for rigid mode with similar k number
			if (~isempty(ind)) % mode found at index ind for rigid case -> transfer
				kk_new(ind,l) = kk(n,l);
				ff_new(ind,l) = ff(n,l);
				BB_new(ind,l) = BB(n,l);
				Acc_new(ind,l) = Acc(n,l);		
			else % no match : specific elastic mode -> store separately
				disp(['No equivalent mode found for : n = ' int2str(nn) ' and l = ' int2str(ll)])
				ispec = ispec + 1;
				kk_spec(ispec,l) = kk(n,l);
				ff_spec(ispec,l) = ff(n,l);
				BB_spec(ispec,l) = BB(n,l);
				Acc_spec(ispec,l) = Acc(n,l);
			end
		end % end n loop
	end % end l loop
	filename_new = [filename '_new'];
	ReadMe_elastic_modes = [ReadMe_elastic_modes char(10) '--> Re-ordered by reorder_elastic_modes.m'];
	[n_max, l_max] = size(kk_new);
	n_max=n_max-1;
	l_max=l_max-1;
	save(filename_new,'kk_new','ff_new','BB_new','Acc_new','ReadMe_elastic_modes', 'parameters','n_max','l_max');
	disp(['re-ordered modes saved in : ' filename_new])
	if (0 == 0)
% derived relevant parameters
		lambda = rho * csound^2 ./ mu / 2.; % lambda(1:2) parameter of Rand and DiMaggio
		kappa_0 = (rho_f./rho) .* (rf./hat); % kappa_0(1:2) parameter of Rand and DiMaggio
		draw_elastic_modes(n_max,l_max,ff_new,csound,rf,lambda,kappa_0,nu);
	end
	filename_spec = [filename '_spec'];
	[n_max, l_max] = size(kk_spec);
	n_max=n_max-1;
	l_max=l_max-1;
	save(filename_spec,'kk_spec','ff_spec','BB_spec','Acc_spec','ReadMe_elastic_modes', 'parameters','n_max','l_max');
	disp(['specific elastic modes saved in : ' filename_spec])
	if (0 == 0)
% derived relevant parameters
		lambda = rho * csound^2 ./ mu / 2.; % lambda(1:2) parameter of Rand and DiMaggio
		kappa_0 = (rho_f./rho) .* (rf./hat); % kappa_0(1:2) parameter of Rand and DiMaggio
		draw_elastic_modes(n_max,l_max,ff_spec,csound,rf,lambda,kappa_0,nu);
	end
end % end test elastic
