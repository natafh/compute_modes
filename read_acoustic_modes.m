function [parameters,n_max,l_max, kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, delta_f_shell,ReadMe_elastic_modes] = read_acoustic_modes(filename)
% --------------------------------------------------------------------------
% 191015 HCN : install get_acoustic_dir
% 190730 HCN : reads a mat file of acoustic mode frequencies and related quantities
% 171017 : lit un fichier mat de modes acoustiques et grandeurs associées
%
acoustic_dir = get_acoustic_dir;
dirname = [acoustic_dir 'compute_modes/Modes/'];
%
filename = [filename '_acoustic_modes.mat'];
load([dirname filename]);
%load([dirname filename],'parameters','n_max', 'l_max', 'kk','ff','BB','C_nl','Acc','gamma_nl','gamma_nlm','g_nl','delta_f_nl','ReadMe_elastic_modes');
%
if ~exist('dk2_nlm')
    dk2_nlm = NaN;
end
%
if ~exist('delta_f_shell') % introduced 190705
    disp('*** read_acoustic_modes warning : no delta_f_shell data in mode file ***')
    disp('    --> set to zero')
    delta_f_shell = zeros(n_max+1,l_max+1);
end
%
if ~isfield(parameters,'e_type') % introduced 190816
    disp('*** read_acoustic_modes warning : e_type undefined in mode file ***')
    disp('    --> set to oblate')
    parameters.e_type{1} = 'oblate';
    parameters.e_type{2} = 'oblate';
end
%
disp([filename ' file loaded from ' dirname]);