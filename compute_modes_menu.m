% -----------------------------------------------------------------
% Program to compute the acoustic modes of a spherical shell cavity
% -----------------------------------------------------------------
% 230623 HCN : enter pressure for gases
% 191015 HCN : install get_acoustic_dir
% 190805 HCN : extension to ellipticities for an acoustic shell *** en test ***
% 190704 HCN : add computation of the shell modes and shell's frequency shift
% 190227 HCN : add Mehl's second order ellipticity correction
% 190205 HCN : add Mehl's first order ellipticity correction
% 190118 HCN : reorganized version for Acc
% 190114 HCN : new version, treating fluid, shells and BC separately
% 190107 HCN : specify properties for ZoRo2
% 171016 HCN : add attenuation corrections (according to Moldover et al, 1986)
% 170809 HCN : corrected bad bug for Ledoux coefficient
% 140203 HCN : added properties for BigSister
% 140124 HCN
% calls function elastic_spherical_shell _modes, which can deal with elastic
% or rigid boundaries, with or without an inner sphere
% set mu(i) and/or hat(i) = Inf to make i-shell rigid
% set rf(1) = 0 to remove the inner sphere
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'compute_modes/drawing_functions'])
%
sphere_type = input(['Enter sphere type (ZoRo2, ZoRo, plexi-full, plexi-shell_10, plexi-shell_15, DTS, 30cm, 60cm, BigSister, )' char(10)],'s');
%
fluid_type = input(['Enter fluid type (air, water, sodium, N2, argon, CO2, SF6, helium)' char(10)],'s');
%
BC_type = input(['Enter boundary treatment (elastic, rigid)' char(10)],'s');
%
% mode numbers
l_max = 18;
n_max = 7;
%
% default values
rf(1) = 0.; % no inner sphere
mu(1) = Inf; % rigid inner sphere
hat(1) = Inf; % dumy
rho(1) = 1.; % dumy
nu(1) = 1.; % dumy
e_flag = 'Mehl'; % use Mehl's formula by default (accounts for inner sphere)
ellipticity(1:2) = 0; % inner/outer shell ellipticity (if any)
e_type(1:2) = {'oblate'}; % ellipticity type ('oblate', 'prolate', etc) by default
if_second_order = false; % no second order ellipticity dk2_nlm by default (only without inner sphere)
%
%% the spherical shells
% -----------------------------------------------
switch sphere_type
%
% values for ZoRo2 alu full spheroid
	case 'ZoRo2'
		r_pol(1) = 0; % no inner sphere (m)
		r_equ(1) = 0; % no inner sphere (m)
        r_pol(2) = 0.190; % (m) polar radius (plan Max 13/10/2017)
        r_equ(2) = 0.200; % (m) equatorial radius (plan Max 13/10/2017)
        e_flag = 'Mehl';
        [rf, ellipticity, e_type] = get_ellipticity(r_equ,r_pol,e_flag);
% (rf(2) = 0.19661 m, ellipticity = 0.05263) from Mehl's formula
% (rf(2) = 0.19655 m, ellipticity = 0.0517) from Dahlen's formula
        if_test = input('use Mehl (2007) second order ellipticity coefficients (y/n) ? ','s');
        if (if_test(1)=='y' || if_test(1) == 'Y') % use Mehl second order ellipticity coefficients
            if_second_order = true;
            disp('*** using Mehl (2007) second order ellipticity coefficients')
        end
%
        disp(['--- ' sphere_type ' is elliptical with :'])
        for ii=1:2
            disp(['       ' e_type{ii} ' with rf(' int2str(ii) ') = ' num2str(1000.*rf(ii)) ' mm, and epsilon(' int2str(ii) ') = ' num2str(ellipticity(ii))])
        end
		hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
		rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
		mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
		nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
%
% values for hypothetical ZoRo2 with a spherical inner sphere
	case 'shell-test'
        r_pol(2) = 0.190; % (m) polar radius (plan Max 13/10/2017)
        r_equ(2) = 0.190; % (m) 
		r_equ(1) = 0.35*r_equ(2); % inner sphere radius (m)
		r_pol(1) = r_equ(1); % spherical inner sphere
		e_flag = 'Mehl';
        [rf, ellipticity, e_type] = get_ellipticity(r_equ, r_pol, e_flag); % *** ne permet pas 2 ellipticités de forme différente ***
% (r_sph = 0.19661 m, ellipticity = 0.05263) from Mehl's formula
%
        disp(['--- ' sphere_type ' is elliptical with :'])
        for ii=1:2
            disp(['       ' e_type{ii} ' with rf(' int2str(ii) ') = ' num2str(1000.*rf(ii)) ' mm, and epsilon(' int2str(ii) ') = ' num2str(ellipticity(ii))])
        end
		hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
		rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
		mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
		nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
%
% values for ZoRo2 David's COMSOL benchmarkalu full spheroid
	case 'ZoRo2-COMSOL'
		rf(1) = 0; % no inner sphere (m)
		rf(2) = 0.2; % (m)
		hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
		rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
		nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
		mu(2) = 71e9/2/(1+nu(2)); % (Pa) shell shear modulus (Young = 71e9)
% values for ZoRo brass full sphere
	case 'ZoRo'
		rf(1) = 0; % no inner sphere (m)
		rf(2) = 0.395/2; % internal radius brass sphere (m) (Adeline's value 160606)
		hat(2) = 0.0025 ; % shell thickness (m) (Adeline's value 160606)
		rho(2) = 8440.; % shell density (kg/m^3) (SERAS for CuZn37 151110)
		mu(2) = 40e9; % % shell shear modulus (Pa) (from CIDEC datasheet for CuZn37 1970)
		nu(2) = 110e9/2/mu(2) - 1 ; % shell Poisson's ratio = E/(2*mu)-1 (E from SERAS for CuZn37 151110)
%
% Aurélien and Ali's plexiglass sphere (no inner sphere)
	case 'plexi-full'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
%
% values for Aurélien's plexiglass sphere with 15cm-diameter inner polystyrene sphere
	case 'plexi-shell_15' 
		rf(1) = 0.15/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
%
% values for Ali's plexiglass sphere with 10cm-diameter inner polystyrene sphere
	case 'plexi-shell_10'
		rf(1) = 0.10/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
%
% values for DTS stainless steel shell with rigid inner sphere
	case 'DTS'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius outer shell (m)
		hat(2) = 0.005 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
%
% values for Dan Lathrop's 30 cm shell
	case '30cm'
		rf(1) = 0.052; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.155; % internal radius 30cm sphere (m)
		hat(2) = 0.0127 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
%
% values for Dan Lathrop's 60 cm shell
	case '60cm' % *** to be completed ***
		rf(1) = 0.1; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.3048; % internal radius brass sphere (m)
		hat(2) = Inf ; % shell thickness (m)
		rho(2) = 1.; % shell density (kg/m^3)
		mu(2) = Inf; % % shell shear modulus (Pa)
		nu(2) = 1 ; % shell Poisson's ratio
%
	case 'BigSister'
		rf(1) = 0.51; % radius of the inner sphere (m)
		rf(2) = 1.46; % internal radius plexi sphere (m)
% 304 stainless steel outer shell
		hat(2) = 0.025 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa
% 304 stainless steel inner shell
		hat(1) = 0.00635 ; % inner shell thickness (m) (from Santiago's mail, Feb 3, 2014)
		rho(1) = rho(2);
		nu(1) = nu(2);
		mu(1) = mu(2);
%
% pour test impedance avec David et Jeremie
	case 'test1' % soft boundary
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1; % internal radius outer shell
		hat(2) = 0.01 ; % shell thickness (m)
		rho(2) = 0.01; % shell density (kg/m^3)
		nu(2) = 0.25 ; % shell Poisson's ratio
		mu(2) = 100.; % shell shear modulus (Pa)
% pour test impedance avec David et Jeremie
	case 'test1bis' % soft boundary with same rho c^2 as test1, but 0.1 its rho c.
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1; % internal radius outer shell
		hat(2) = 0.01 ; % shell thickness (m)
		rho(2) = 0.0001; % shell density (kg/m^3)
		nu(2) = 0.25 ; % shell Poisson's ratio
		mu(2) = 100.; % shell shear modulus (Pa)
% pour test impedance avec David et Jeremie
	case 'test2' % very soft boundary
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1; % internal radius outer shell
		hat(2) = 0.01 ; % shell thickness (m)
		rho(2) = 0.01; % shell density (kg/m^3)
		nu(2) = 0.25 ; % shell Poisson's ratio
		mu(2) = 1.; % shell shear modulus (Pa)
	otherwise
		disp(['Unknown sphere case : ' sphere_type])
end
%
%% the fluid
% -----------------------------------------------
switch fluid_type
    case {'air','air-COMSOL','N2','argon','CO2','SF6','helium'}
        gas_name = fluid_type;
        temperature = 20.; % (Celsius) gas temperature
%        pressure_bar = 1.013; % [bar] gas pressure
        pressure_bar = input(['Enter gas pressure [bar] (ex: 3) : ' char(10)]);
        pressure = pressure_bar * 1e5; % [Pa] pressure
        [csound, rho_f, nu_f, kappa_f, Gruneisen, Prandtl, nu_b] = gas_properties(gas_name, temperature, pressure);
%		csound = 343.19; % sound velocity in air @ 20°C (m/s)
%		Prandtl = nu_f/kappa_f ; % Prandtl number
    case 'water'
        temperature = 20.; % (Celsius) water temperature
		csound = 1481.; % sound velocity in water at 20°C (m/s)
		rho_f = 998.2; % water density at 20°C (kg/m^3)
		Gruneisen = 1.4; % *** Gruneisen parameter = C_P/C_V
		Prandtl = 6.99 ; % Prandtl number of water at 20°C
		nu_f = 1.e-6; % (ms^-2) kinematic viscosity of water at 20°C
		nu_b = 3*nu_f; % (ms^-2) kinematic bulk viscosity of water (Holmes et al, 2011)
    case 'sodium'
        temperature = 125.; % (Celsius) sodium temperature
		csound = 2500.; % sound velocity in sodium (m/s)
		rho_f = 980. ; % sodium density (kg/m^3)
		Gruneisen = 1.138; % Gruneisen parameter = C_P/C_V ?
		Prandtl = 0.01 ; % Prandtl number of sodium at 125°C
		nu_f = 6.67e-7; % (ms^-2) kinematic viscosity of sodium at 125°C
		nu_b = 2.3*nu_f; % (ms^-2) kinematic bulk viscosity of sodium (Kim et al, 1971; Awasthi et al, 1985)
    case 'test' %
        temperature = 20.; % (Celsius) water temperature
		csound = 1.; % sound velocity (m/s)
		rho_f = 1.; % water density at 20°C (kg/m^3)
		Gruneisen = 1.4; % *** Gruneisen parameter = C_P/C_V
		Prandtl = 1. ; % Prandtl number
		nu_f = 1.e-6; % (ms^-2) kinematic viscosity
		nu_b = nu_f; % (ms^-2) kinematic bulk viscosity
	otherwise
		disp(['Unknown fluid case : ' fluid_type])
end
%
%% the mechanical boundary condition
% -----------------------------------------------
mu_save = mu; % save actual mu for shell mode computation
switch BC_type
    case 'rigid'
        mu(1) = Inf; % rigid inner sphere
        mu(2) = Inf; % rigid outer sphere
    case 'elastic'
	otherwise
		disp(['Unknown boundary condition : ' BC_type])

end
%
%% build file name
if exist('pressure_bar')
    if (pressure_bar == 1)
        full_name = [sphere_type '-' fluid_type '-' BC_type];
    else
        full_name = [sphere_type '-' fluid_type '_' num2str(pressure_bar) 'bar' '-' BC_type];
    end
else
    full_name = [sphere_type '-' fluid_type '-' BC_type];
end
%
% ---------------------------------------------------------
%% computation of the ideal elastic shell modes
% ---------------------------------------------------------
[kk, ff, BB] = elastic_spherical_shell_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max);
%
draw_fl = input('draw frequency versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot mode frequencies if wished
	if ~exist('h_f_vs_l')
		h_f_vs_l = figure('name','mode frequencies','position',[100 100 600 800]);
	end
	draw_mode_frequencies(h_f_vs_l,n_max,l_max,ff,csound,full_name);
end
%
% --------------------------------------------------------------------------------
%% computation of the modes of the elastic shell (from Mehl, 1985) installed 190625
% --------------------------------------------------------------------------------
draw_fl = input('---> compute (slow) and overlay elastic shell modes (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % overlay shell mode frequencies if wished
    n_max_shell = 1; % higher n modes have very high frequencies
    l_max_shell = l_max;
    [kk_shell, ff_shell, V_P] = elastic_shell_modes(rf, hat, mu_save, nu, rho, n_max_shell, l_max_shell);
    if exist('h_f_vs_l')
        hold on
        overlay_shell_modes(h_f_vs_l,n_max_shell,l_max_shell,ff_shell);
    else
        disp('*** no frequency vs l figure found : plot elastic shell modes alone')
		h_fig = figure('name','shell mode frequencies','position',[100 100 600 800]);
        draw_shell_modes(h_fig,n_max_shell,l_max_shell,ff_shell,V_P,full_name);
    end
end
% --------------------------------------------------------------------------------
%% computation of the frequency shift due to the elastic shell (from Mehl, 1985) installed 190625
% --------------------------------------------------------------------------------
if (0==0)
    bar = 1 + hat(2)./rf(2);
	V_P = sqrt(mu_save(2)./rho(2) .* 2*(1-nu(2))./(1-2*nu(2))); % P-wave velocity in the shell
    c_ratio = V_P./csound;
    rho_ratio = rho(2)./rho_f;
%
    for nn=0:n_max
        nnn=nn+1;
        for ll=1:l_max
            lll=ll+1;
            ka = kk(nnn,lll);
            delta_f_shell(nnn,lll) = ff(nnn,lll) * approximate_shell_deltaf(ll,ka,nu(2),bar,c_ratio, rho_ratio);
        end
    end
%
    draw_fl = input('draw shell frequency shift versus l figure (y/n) ? ','s');
    if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot mode frequencies if wished
	    if ~exist('h_shell_shift')
            h_shell_shift = figure('name','acoustic mode frequency shift due to elastic shell','position',[100 100 600 800]);
	    end
% *** adapt for plotting frequency shift ***
	    draw_mode_frequencies(h_shell_shift,n_max,l_max,delta_f_shell,csound,full_name);
% ****
    end
end
% ----------------------------------------------------------------
%% computation of the acceleration coefficients (placed here 190118)
% ----------------------------------------------------------------
% acceleration/wall pressure ratio from equation (15) 
% of Rand and DiMaggio (1967)
% Acc(nn+1,ll+1) = (m/s^2/Pa) Acceleration/Pressure ratio for mode nnS_ll 
% Note : Acc is independent of rho_f although it does not appear like so
Acc = acceleration_coefficient(rho_f, rf, kk, BB);
draw_fl = input('draw acceleration ratios Acc versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot acceleration ratios if wished
	if ~exist('h_Acc_vs_l')
		h_Acc_vs_l = figure('name','acceleration ratios','position',[100 100 600 800]);
	end
    draw_acceleration_ratios(h_Acc_vs_l,n_max,l_max,Acc,full_name);
end
%
% ---------------------------------------------------------
%% computation of the Ledoux coefficients (installed 160519)
% ---------------------------------------------------------
C_nl = Ledoux_coefficient(rf, kk, BB);
draw_fl = input('draw Ledoux coefficients C_nl versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot Ledoux coefficients if wished
	if ~exist('h_Ledoux_vs_l')
		h_Ledoux_vs_l = figure('name','Ledoux coefficients','position',[100 100 600 800]);
	end
	if_compens = input('compensate with (l+1) factor (y/n) ? ','s');
	if (if_compens(1)=='y' || if_compens(1) == 'Y') % plot compensate by l(l+1) factor
		draw_Ledoux_coefficients_compens(h_Ledoux_vs_l ,n_max,l_max,C_nl,full_name);
	else
		draw_Ledoux_coefficients(h_Ledoux_vs_l ,n_max,l_max,C_nl,full_name);
	end
end
%
% --------------------------------------------------------------
%% computation of the ellipticity coefficients (installed 170810)
% Mehl (2007)'s second order installed 190227
% extension for spheroidal acoustic shell 190805
% --------------------------------------------------------------
switch e_flag
    case 'Mehl' % *** in test with an inner sphere : assumes same ellipticity type *** 
        [gamma_nl_i, gamma_nl] = ellipticity_coefficient_Mehl(rf,kk,BB,e_type{2});
    case 'Dahlen'
        [gamma_nl_i, gamma_nl] = ellipticity_coefficient_1976(rf,kk,BB); % OK with inner sphere
end
%
if if_second_order
    disp('*** computing Mehl (2007) second order ellipticity coefficients')
    dk2_nlm = ellipticity_coefficient_Mehl_second_order(rf,kk,BB,e_type{2}); % no inner sphere
else
    dk2_nlm = NaN;
end
%
% draw first order ellipticity coefficients gamma_nl vs l
% -------------------------------------------------------
draw_fl = input('draw first-order ellipticity coefficients gamma_nl versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot (first-order) ellipticity coefficients if wished
	if ~exist('h_ellipticity_vs_l')
		h_ellipticity_vs_l = figure('name','ellipticity coefficients','position',[100 100 600 800]);
	end
	if_compens = input('multiply by (l+1)^2 factor (y/n) ? ','s');
	if (if_compens(1)=='y' || if_compens(1) == 'Y') % plot compensate by l^2
		draw_ellipticity_coefficients_compens(h_ellipticity_vs_l, n_max, l_max, gamma_nl, full_name);
	else
		draw_ellipticity_coefficients(h_ellipticity_vs_l, n_max,l_max,gamma_nl, full_name);
	end
end
%
% draw second order ellipticity coefficients dk2_nlm vs l
% -------------------------------------------------------
if if_second_order
    draw_fl = input('draw second-order ellipticity coefficients dk2_nlm versus l figure (y/n) ? ','s');
    if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot (second-order) ellipticity coefficients if wished
        if ~exist('h_second_order_m')
            h_second_order_m = figure('name','second order ellipticity coefficients','position',[100 100 600 800]);
        end
        m_value = input('For all n. For which m value ? ');
		draw_ellipticity_second_order_given_m(h_second_order_m, n_max, l_max, m_value, dk2_nlm, full_name);
        if ~exist('h_second_order_n')
            h_second_order_n = figure('name','second order ellipticity coefficients','position',[100 100 600 800]);
        end
        n_value = input('For all m. For which n value ? ');
		draw_ellipticity_second_order_given_n(h_second_order_n, n_max, l_max, n_value, dk2_nlm, full_name);
    end
end
%
% ---------------------------------------------------------
%% computation of the attenuation coefficients (installed 171016)
% ---------------------------------------------------------
[g_nl, delta_f_nl] = attenuation_half_width(rf, kk, ff, Gruneisen, Prandtl, nu_f, nu_b);
draw_fl = input('draw attenuation half-widths g_nl versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot attenuation half-widths if wished
	if (~exist('h_attenuation_vs_l'))
		h_attenuation_vs_l = figure('name','attenuation coefficients','position',[100 100 600 800]);
	end
	draw_attenuation_coefficients(h_attenuation_vs_l,n_max,l_max,g_nl, full_name);
end
%
% ---------------------------------------------------------
%% save results
% ---------------------------------------------------------
save_results = input(['Save results ? (y/n) ' char(10)],'s');
if (save_results == 'y' || save_results == 'Y')
	ReadMe_elastic_modes = [ ...
	'This file contains the results of the computation of acoustic modes in a spherical fluid shell ' char(10)...
	'enclosed between two solid shells (1=inner, 2=outer), which can be either rigid or elastic. ' char(10) ...
	'The elastic_spherical_shell_modes.m function used is based on the analysis ' ...
	'of Rand and DiMaggio (1967).' char(10) char(10) ...
	'The outputs are : ' char(10) ...
	'	kk(n+1,l+1) : the adimensional root k of mode nSl' char(10) ...
	'	ff(n+1,l+1) : the dimensional frequency f (Hz) of mode nSl' char(10) ...
	'	BB(n+1,l+1) : the adimensional ratio of the y over j Bessel function for mode nSl' char(10) ...
	'	C_nl(n+1,l+1) : the Ledoux coefficient for mode nSl' char(10) ...
	'	gamma_nl(n+1,l+1) : the ellipticity coefficient for mode nSl (see Dahlen, 1976)' char(10) ...
	'	dk2_nlm(n+1,l+1,l+1) : the second order ellipticity coefficient for singlet nSlm (see Mehl, 2007); use Mehl_apply_second_order.m' char(10) ...
	'	Acc(n+1,l+1) : the dimensional Acceleration/Wall-pressure ratio (m/s^2/Pa) of the outer shell' char(10) ...
	'	g_nl(n+1,l+1) : the attenuation half-width for mode nSl (see Moldover et al, 1986)' char(10) ...
	'	delta_f_nl(n+1,l+1) : the frequency correction for mode nSl due to attenuation (see Moldover et al, 1986)' char(10) char(10) ...
	'Also stored : ' char(10) ...
	'	n_max : the maximum radial wave number n' char(10) ...
	'	l_max : the maximum angular wave number l' char(10) ...
	'	ReadMe_elastic_modes : the present message' char(10) ...
	'	parameters : a structure collecting the other input parameters : ' char(10) ...
	'		sphere_type, fluid_type, BC_type, temperature, csound,' char(10) ...
	'		rho_f, rf, hat, rho, mu, nu, if_Mehl, if_second_order (see function for definitions).' char(10) ...
	'		Grunseisen, Prandtl, nu_f of the fluid for attenuation.' char(10) ...
	'		and ellipticity for a spheroid.' char(10) ...
	'NB : rf(1) = 0 means no inner sphere, and mu(i) = Inf means rigid "i" boundary.' char(10) ...
	'Note that the n order of modes is disrupted when boundaries are elastic : ' char(10) ...
	'you might want to re-order them using the modes computed for rigid boundaries as a reference.'
	];
	parameters.sphere_type = sphere_type;
	parameters.fluid_type = fluid_type;
	parameters.BC_type = BC_type;
	parameters.temperature = temperature;
	if exist('pressure')
    	parameters.pressure = pressure;
	end
	parameters.csound = csound;
	parameters.rho_f = rho_f;
	parameters.rf = rf;
	parameters.hat = hat;
	parameters.rho = rho;
	parameters.mu = mu;
	parameters.mu_save = mu_save;
	parameters.nu = nu;
	parameters.Gruneisen = Gruneisen;
	parameters.Prandtl = Prandtl;
	parameters.nu_f = nu_f;
	parameters.ellipticity = ellipticity;
	parameters.e_flag = e_flag;
	parameters.e_type = e_type;
	parameters.if_second_order = if_second_order;
%
	filename = full_name;
	write_acoustic_modes(filename,parameters,n_max,l_max,kk,ff,BB, C_nl,Acc,gamma_nl, dk2_nlm,g_nl,delta_f_nl,delta_f_shell,ReadMe_elastic_modes);
end
