function write_acoustic_modes(filename,parameters,n_max,l_max, kk,ff,BB,C_nl,Acc,gamma_nl, dk2_nlm,g_nl,delta_f_nl,delta_f_shell,ReadMe_elastic_modes)
% --------------------------------------------------------------------------
% 191015 HCN : install get_acoustic_dir
% 190730 HCN : writes a mat file of acoustic mode frequencies and related quantities
% 171017 : ecrit un fichier mat de modes acoustiques et grandeurs associées
%
acoustic_dir = get_acoustic_dir;
dirname = [acoustic_dir 'compute_modes/Modes/'];
%
do_save = true;
filename = [filename '_acoustic_modes.mat'];
if (exist([dirname filename])==2) % file exists
    overwrite = input(['overwrite ' dirname filename ' ? (yes/no)'],'s');
    if (overwrite(1)=='y' || overwrite(1) == 'Y')
    else
        do_save = false;
    end
end
%
if do_save
    save([dirname filename],'parameters','n_max', 'l_max', 'kk','ff','BB','C_nl','Acc','gamma_nl','dk2_nlm','g_nl','delta_f_nl','delta_f_shell','ReadMe_elastic_modes');
    disp([filename ' file saved in ' dirname]);
end