function fc =eigenfit2_comsol(n,l,m,ee,fadim)
% -------------------------------------------------------------------------
%% Author: D. Cebron (ISTerre, CNRS)  -- Oct. 2019
% With a rotating spheroid of polar radius rpol and equatorial radius req,
% we use the length scale req & the sound speed c as the velocity scale
% and no-slip isothermal bounday condition to fit the numerical COMSOL
% results with a degree 2 polynomial in ee and fadim (assumed to be <<1)
% -------------------------------------------------------------------------
% (n,l,m): the wave numbers using seismology convention
% ee=1-rpol/req : spheroid ellipticity
% fadim : dimensionless rotation rate frequency
% -------------------------------------------------------------------------
% fc : dimensionless acoustic complex eigenfrequency (w/ diffusion)
% -------------------------------------------------------------------------

fc=NaN;
load database2_zoro.mat;
% Unit
unit=scale_comsol;

km=l+m+1;
if(abs(m)<=l)
    if(n==0)
        if(l>length(n0_e1(:,1))-1)
            display('NOT YET IN THE DATABASE');
        else
            f0=n0_f0(l+1,km)+n0_e1(l+1,km)*ee+n0_e2(l+1,km)*ee^2+n0_e3(l+1,km)*ee^3+n0_f1(l+1,km)*fadim+n0_f2(l+1,km)*fadim^2+n0_f3(l+1,km)*fadim^3+n0_e1f1(l+1,km)*fadim*ee+n0_e2f1(l+1,km)*fadim*ee^2+n0_e1f2(l+1,km)*fadim^2*ee;
            damp=n0_f0_damp(l+1,km)+n0_e1_damp(l+1,km)*ee+n0_e2_damp(l+1,km)*ee^2+n0_e3_damp(l+1,km)*ee^3+n0_f1_damp(l+1,km)*fadim+n0_f2_damp(l+1,km)*fadim^2+n0_f3_damp(l+1,km)*fadim^3+n0_e1f1_damp(l+1,km)*fadim*ee+n0_e2f1_damp(l+1,km)*fadim*ee^2+n0_e1f2_damp(l+1,km)*fadim^2*ee;
            fc=(f0+1i*damp)*unit;
        end
    elseif(n==1)
        if(l>length(n1_e1(:,1))-1)
            display('NOT YET IN THE DATABASE');
        else       
            f0=n1_f0(l+1,km)+n1_e1(l+1,km)*ee+n1_e2(l+1,km)*ee^2+n1_e3(l+1,km)*ee^3+n1_f1(l+1,km)*fadim+n1_f2(l+1,km)*fadim^2+n1_f3(l+1,km)*fadim^3+n1_e1f1(l+1,km)*fadim*ee+n1_e2f1(l+1,km)*fadim*ee^2+n1_e1f2(l+1,km)*fadim^2*ee;
            damp=n1_f0_damp(l+1,km)+n1_e1_damp(l+1,km)*ee+n1_e2_damp(l+1,km)*ee^2+n1_e3_damp(l+1,km)*ee^3+n1_f1_damp(l+1,km)*fadim+n1_f2_damp(l+1,km)*fadim^2+n1_f3_damp(l+1,km)*fadim^3+n1_e1f1_damp(l+1,km)*fadim*ee+n1_e2f1_damp(l+1,km)*fadim*ee^2+n1_e1f2_damp(l+1,km)*fadim^2*ee;
            fc=(f0+1i*damp)*unit;
        end
    elseif(n==2)
        if(l>length(n2_e1(:,1))-1)
            display('NOT YET IN THE DATABASE');
        else
            f0=n2_f0(l+1,km)+n2_e1(l+1,km)*ee+n2_e2(l+1,km)*ee^2+n2_e3(l+1,km)*ee^3+n2_f1(l+1,km)*fadim+n2_f2(l+1,km)*fadim^2+n2_f3(l+1,km)*fadim^3+n2_e1f1(l+1,km)*fadim*ee+n2_e2f1(l+1,km)*fadim*ee^2+n2_e1f2(l+1,km)*fadim^2*ee;
            damp=n2_f0_damp(l+1,km)+n2_e1_damp(l+1,km)*ee+n2_e2_damp(l+1,km)*ee^2+n2_e3_damp(l+1,km)*ee^3+n2_f1_damp(l+1,km)*fadim+n2_f2_damp(l+1,km)*fadim^2+n2_f3_damp(l+1,km)*fadim^3+n2_e1f1_damp(l+1,km)*fadim*ee+n2_e2f1_damp(l+1,km)*fadim*ee^2+n2_e1f2_damp(l+1,km)*fadim^2*ee;
            fc=(f0+1i*damp)*unit;
        end
    elseif(n==3)
        if(l>length(n3_e1(:,1))-1)
            display('NOT YET IN THE DATABASE');
        else
            f0=n3_f0(l+1,km)+n3_e1(l+1,km)*ee+n3_e2(l+1,km)*ee^2+n3_e3(l+1,km)*ee^3+n3_f1(l+1,km)*fadim+n3_f2(l+1,km)*fadim^2+n3_f3(l+1,km)*fadim^3+n3_e1f1(l+1,km)*fadim*ee+n3_e2f1(l+1,km)*fadim*ee^2+n3_e1f2(l+1,km)*fadim^2*ee;
            damp=n3_f0_damp(l+1,km)+n3_e1_damp(l+1,km)*ee+n3_e2_damp(l+1,km)*ee^2+n3_e3_damp(l+1,km)*ee^3+n3_f1_damp(l+1,km)*fadim+n3_f2_damp(l+1,km)*fadim^2+n3_f3_damp(l+1,km)*fadim^3+n3_e1f1_damp(l+1,km)*fadim*ee+n3_e2f1_damp(l+1,km)*fadim*ee^2+n3_e1f2_damp(l+1,km)*fadim^2*ee;
            fc=(f0+1i*damp)*unit;
        end
    else
        display('NOT YET IN THE DATABASE');
    end
else
    display('WE MUST HAVE abs(m)<=l');
end