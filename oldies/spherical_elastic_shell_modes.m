% -----------------------------------------------------------------------
function [kk, ff] = spherical_elastic_shell_modes(r_o, h_s, rho, rho_s, csound, mu, nu, n_max, l_max)
% -----------------------------------------------------------------------
% computes the 'k' kk and frequency ff of acoustic modes (nn,ll) in a sphere of fluid shell
% enclosed within an elastic spherical shell
% *** No inner sphere at the moment ***
% note : ff = kk * csound/(2 pi r_o)
% 140123 : coded from paper by Rand and DiMaggio (1967), following Aurélien modes_sphere matlab program
%
% inputs :
%	r_o = radius of the outer sphere (m)
%	h_s = thickness of the elastic shell (m)
%	rho = density of the fluid (kgm^-3)
%	rho_s = density of the elastic shell (kgm^-3)
%	csound = sound velocity (m/s)
%	mu = shear modulus of the elastic shell (Pa)
%	nu = Poisson's ratio of the elastic shell
%	n_max = max radial mode number
%	l_max = max angular mode number
%
% outputs :
%	kk(nn,ll) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	ff(nn,ll) = frequency of mode nnS_ll (Hz)
%  
%    Re = 420e-3/2;
%    csound = 2500;

	disp('Welcome in spherical_elastic_shell_modes, which computes the acoustic modes of an elastic spherical shell')	    
% derived relevant parameters
lambda = rho_s * csound^2 / mu / 2.; % lambda parameter of Rand and DiMaggio
kappa_0 = (rho/rho_s) * (r_o/h_s); % kappa_0 parameter of Rand and DiMaggio

% *** debug ***
if (0 == 1)
% impose values from Rand and DiMaggio to compare
	disp('*** cheat : use Rand and DiMaggio values for their figure 6 :')
	nu = 0.33
	lambda = 0.103
	kappa_0 = 17.6
	csound = 2*pi*r_o % so that ff=kk...
	n_max = 11
	l_max = 16
end
% *************
	    
% full sphere
f_sphere = @(l,x) elastic_spherical_shell(l,x,lambda,kappa_0,nu);

kk = f_modes(f_sphere, n_max, l_max); % 'k' of modes (nn,ll)

ff = kk*csound/(2*pi*r_o); % frequency of modes (nn,ll)
	
	if (0 == 0) % plot mode frequencies if wished
		coul = 'kbrmgkbrmgkbrmgkbrmg';
		marker = 'ox+vsphox+vspho';
		figure('name','mode frequencies')
		title(['_nS_l mode frequencies for elastic spherical shell r_o=' num2str(r_o) ', c=' num2str(csound)])
		xlabel('mode angular number l')
		ylabel('mode frequency (Hz)')
		box on
		grid on
		hold on
		for nn=0:n_max-1
			nnn=nn+1;
			plot((1:l_max+1)-1,ff(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn))
			to_legend = int2str(nn);
			if (nn == 0)
				legend_cell={['n=' to_legend]};
			else
				legend_cell=[legend_cell to_legend];
			end
		end
		legend(legend_cell)
	end
end

% ---------------------------------
function z = f_modes(f, nmax, lmax)
% ---------------------------------
	z = zeros(nmax,lmax);	
	for l = 0:lmax
		z(:,l+1) = fzeros(@(x) f(l,x), l+1, Inf, nmax, 1);
	end	    
% correction pour le mode (0,0)
	z(:,1)=[0; z(1:end-1,1)];
end

% -------------------------------------------
function z = fzeros(f, x0, xmax, nmax, delta)
% -------------------------------------------
%% fzeros : résolution de f(x) = 0
% f = fonction dont on cherche les zéros
% x0 = point de départ (début intervalle : doit être inférieur à tous les
% zéros recherchés)
% xmax = fin de l'intervalle de recherche (peut être +Inf)
% nmax = nombre de zéros recherchés (peut être +Inf)
% delta = écart minimal entre les zéros
%
	threshold = 1;   % limite déterminant si deux valeurs trouvées sont égales
	alloc_step = 128;   % si le nombre de zéros n'est pas spécifié, on alloue un tableau par tranches arbitraires
    
	if nmax < Inf
		z = zeros(nmax,1);
	else
		z = zeros(alloc_step,1);
	end

	n = 1;
	x = x0;
	while x < xmax && n <= nmax
% recherche le zéro le plus proche
		[xn, ~, err] = fzero(f, x, optimset('Display','off'));
%       [xn, ~, err] = fsolve(f, x); % 130829 HCN : replace fzero by fsolve
        
% vérifie si on a trouvé une nouvelle valeur
	if err == 1 && (n == 1 || abs(xn - z(n - 1)) > threshold)
		z(n) = xn;
		n = n + 1;
% augmente la taille du tableau si nécessaire
			if nmax == Inf && mod(n, alloc_step) == 0
				z = [z ; zeros(alloc_step, 1)];
			end
			x = xn;
		end
		x = x + delta;
	end
	z(n:end) = [];
end

%% ================== fonction for elastic shell =================== %
% -----------------------------------
function f = elastic_spherical_shell(n,z,lambda,kappa_0,nu);
% -----------------------------------
% equation (21) of Rand and DiMaggio
	crochet1 = (1-nu)/(1+nu)*lambda*z.^2 - 2;
	accolade = (crochet1*(1 + lambda*z.^2) + n*(n+1)*(1 - lambda*z.^2/(1+nu)));
	crochet2 = n - z*besselj(n+3/2,z)./besselj(n+1/2,z);
	crochet3 = (1-nu)/(1+nu)*(1 + lambda*z.^2) - n*(n+1)/(1+nu);
	f = accolade*crochet2 + kappa_0*lambda*z.^2*crochet3;
end
%% ================== fonctions de Bessel sphériques =================== %
% -----------------------------------
function f=d_spherical_bessel_j(n, x)
% -----------------------------------
% d/dx j(n,x)
% 170810 HCN : factor pi/2 bug corrected + more compact
	f = sqrt(pi./(2*x)).*(n*besselj(n+1/2,x)./x - besselj(n+3/2,x));
end
% -----------------------------------
function f=d_spherical_bessel_y(n, x)
% -----------------------------------
% d/dx y(n,x)
% 170810 HCN : factor pi/2 bug corrected + more compact
	f = sqrt(pi./(2*x)).*(n*bessely(n+1/2,x)./x - bessely(n+3/2,x));
end


% -----------------------------------
function f=d_spherical_bessel_j(n, x)
% -----------------------------------
% d/dx j(n,x)
	f = sqrt(1/2/pi)*(-besselj(n+1/2,x).*(x.^(-3/2)) + (besselj(n-1/2,x)-besselj(n+3/2,x)).*(x.^(-1/2)));
end
% -----------------------------------
function f=d_spherical_bessel_y(n, x)
% -----------------------------------
% d/dx y(n,x)
	f = sqrt(1/2/pi)*(-bessely(n+1/2,x).*(x.^(-3/2)) + (bessely(n-1/2,x)-bessely(n+3/2,x)).*(x.^(-1/2)));
end
% ---------------------------------
function f=spherical_bessel_j(n, x)
% ---------------------------------
% j(n,x) = sqrt(2/pi x) * J(n + 1/2, x)
	f = sqrt(pi/2)*besselj(n+1/2,x)./sqrt(x);
end
% ---------------------------------
function f=spherical_bessel_y(n, x)
% ---------------------------------
% y(n,x) = sqrt(2/pi x) * Y(n + 1/2, x)
	f = sqrt(pi/2)*bessely(n+1/2,x)./sqrt(x);
end
%% ================== =============================== =================== %