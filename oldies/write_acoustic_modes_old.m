function write_acoustic_modes(octave,filename,parameters,n_max,l_max, kk,ff,BB,C_nl,Acc,gamma_nl,g_nl,delta_f_nl,ReadMe_elastic_modes);
% 171017 : ecrit un fichier mat de modes acoustiques et grandeurs associées
%
if (octave==0) % matlab sur serveur
	dirname = '/data/geodynamo/natafh/Dynamo/Acoustics/elastic_shell/Modes/';
elseif (octave==1) % octave sur Mac
	dirname = '/Users/natafhenri-claude/Documents/Recherche/Acoustics/elastic_shell/Modes/';
end
%
do_save = true;
filename = [filename '_acoustic_modes.mat'];
if (exist([dirname filename])==2) % file exists
    overwrite = input(['overwrite ' dirname filename ' ? (yes/no)'],'s');
    if (overwrite(1)=='y' || overwrite(1) == 'Y')
    else
        do_save = false;
    end
end
%
if do_save
    save([dirname filename],'parameters','n_max', 'l_max', 'kk','ff','BB','C_nl','Acc','gamma_nl','g_nl','delta_f_nl','ReadMe_elastic_modes');
    disp([filename ' file saved in ' dirname]);
end