function [parameters,n_max,l_max, kk,ff,BB,C_nl,Acc,gamma_nl,g_nl,delta_f_nl, ReadMe_elastic_modes] = read_acoustic_modes(octave,filename);
% 171017 : lit un fichier mat de modes acoustiques et grandeurs associées
%
if (octave==0) % matlab sur serveur
	dirname = '/data/geodynamo/natafh/Dynamo/Acoustics/elastic_shell/Modes/';
elseif (octave==1) % octave sur Mac
	dirname = '/Users/natafhenri-claude/Documents/Recherche/Acoustics/elastic_shell/Modes/';
end
%
filename = [filename '_acoustic_modes.mat'];
load([dirname filename],'parameters','n_max', 'l_max', 'kk','ff','BB','C_nl','Acc','gamma_nl','g_nl','delta_f_nl','ReadMe_elastic_modes');
disp([filename ' file loaded from ' dirname]);