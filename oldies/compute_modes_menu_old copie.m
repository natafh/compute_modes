% ------------------------------------------------------------
% Program to compute the acoustic modes of our various set-ups
% ------------------------------------------------------------
% 181221 HCN : *** ZoRo2 : specify properties ***
% 171016 HCN : add attenuation corrections (according to Moldover et al, 1986)
% 170809 HCN : corrected bad bug for Ledoux coefficient
% 140203 HCN : added properties for BigSister
% 140124 HCN
% calls function elastic_spherical_shell _modes, which can deal with elastic
% or rigid boundaries, with or without an inner sphere
% set mu(i) and/or hat(i) to make i-shell rigid
% set rf(1) = 0 to remove the inner sphere 
%
if (exist('octave')==1) % variable octave déjà définie
else % définir octave
	octave = 0; % sur serveur avec Matlab
%	octave = 1; % sur Mac avec Octave
end
%
global h_f_vs_l % declare figure handle h_f_vs_l as global
global h_Ledoux_vs_l % declare figure handle h_Ledoux_vs_l as global
global h_ellipticity_vs_l % declare figure handle h_ellipticity_vs_l as global
global h_attenuation_vs_l % declare figure handle h_attenuation_vs_l as global
disp('NB: use clear global h_f_vs_l h_Ledoux_vs_l h_ellipticity_vs_l h_attenuation_vs_l to clear figure handles')

sphere_type = input(['Enter sphere type (ZoRo2-rigid-air, ZoRo-air, plexi-full, plexi-full-test, plexi-shell_10, plexi-shell_15, DTS-air, DTS-sodium, 30cm, 30cm-water, BigSister, )' char(10)],'s');
%
rf(1) = 0.; % no inner sphere
mu(1) = Inf; % rigid inner sphere
hat(1) = Inf; % dumy
rho(1) = 1.; % dumy
nu(1) = 1.; % dumy
% attenuation parameters
Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
Prandtl = 0.713 ; % Prandtl number of air at 20°C
nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
ellipticity = 0; % ellipticity (if any)
%
switch sphere_type
% values for comparison with David's COMSOL results : normalized rigid full sphere
	case 'test-60cm'
		rf(1) = 0.1; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.3048; % internal radius brass sphere (m)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = Inf ; % shell thickness (m)
		rho(2) = 1.; % shell density (kg/m^3)
		mu(2) = Inf; % % shell shear modulus (Pa)
		nu(2) = 1 ; % shell Poisson's ratio
% values for ZoRo brass full sphere (elastic case)
	case 'test-rigid'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1; % internal radius brass sphere (m)
		csound = 1; % sound velocity (m/s)
%
		rho_f = 1. ; % air density (kg/m^3)
		hat(2) = Inf ; % shell thickness (m)
		rho(2) = 1.; % shell density (kg/m^3)
		mu(2) = Inf; % % shell shear modulus (Pa)
		nu(2) = 1 ; % shell Poisson's ratio
% values for ZoRo brass full sphere (elastic case)
	case 'ZoRo-air'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.395/2; % internal radius brass sphere (m) (Adeline's value 160606)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.0025 ; % shell thickness (m) (Adeline's value 160606)
		rho(2) = 8440.; % shell density (kg/m^3) (SERAS for CuZn37 151110)
		mu(2) = 40e9; % % shell shear modulus (Pa) (from CIDEC datasheet for CuZn37 1970)
		nu(2) = 110e9/2/mu(2) - 1 ; % shell Poisson's ratio = E/(2*mu)-1 (E from SERAS for CuZn37 151110)
% values for ZoRo brass full sphere (rigid case)
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
	case 'ZoRo-rigid-air'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.395/2; % internal radius brass sphere (m) (Adeline's value 160606)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = Inf ; % shell thickness (m) (Adeline's value 160606)
		rho(2) = 8440.; % shell density (kg/m^3) (SERAS for CuZn37 151110)
		mu(2) = Inf; % % shell shear modulus (Pa) (from CIDEC datasheet for CuZn37 1970)
		nu(2) = 110e9/2/mu(2) - 1 ; % shell Poisson's ratio = E/(2*mu)-1 (E from SERAS for CuZn37 151110)
% values for Aurélien's plexiglass sphere (elastic case)
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
	case 'ZoRo2-rigid-air'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
% compute rf(2) as Dahlen's reference radius from r_pol and r_equ
        r_pol = 0.190; % (m) polar radius (plan Max 13/10/2017)
        r_equ = 0.200; % (m) equatorial radius (plan Max 13/10/2017)
        r_o = 3./(1./r_pol + 2./r_equ); % (= 0.19655 m) from Dahlen's formula
        ellipticity = 3*(1-r_equ./r_pol)./(2+r_equ./r_pol); % (= 0.0517 from Dahlen's formula)
        disp([sphere_type ' is elliptical with r_o = ' num2str(1000.*r_o) ' mm, and epsilon = ' num2str(ellipticity)])
		rf(2) = r_o;
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = Inf ; % shell thickness (m) ***
		rho(2) = 2810.; % shell density (kg/m^3) ***
		mu(2) = Inf; % % shell shear modulus (Pa) ***
		nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
% values for air
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
% NB : shell thermal conductivity
%       160 % (W m-1 K-1)
	case 'ZoRo2-air' % *** a preciser 181221 ***
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
% compute rf(2) as Dahlen's reference radius from r_pol and r_equ
        r_pol = 0.190; % (m) polar radius (plan Max 13/10/2017)
        r_equ = 0.200; % (m) equatorial radius (plan Max 13/10/2017)
        r_o = 3./(1./r_pol + 2./r_equ); % (= 0.19655 m) from Dahlen's formula
        ellipticity = 3*(1-r_equ./r_pol)./(2+r_equ./r_pol); % (= 0.0517 from Dahlen's formula)
        disp([sphere_type ' is elliptical with r_o = ' num2str(1000.*r_o) ' mm, and epsilon = ' num2str(ellipticity)])
		rf(2) = r_o;
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % (kg/m^3) air density
		hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
		rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
		mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
		nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
% values for air
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
% NB : shell thermal conductivity
%       160 % (W m-1 K-1)
	case 'plexi-full-test'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = Inf; % shell shear modulus (Pa) (Idemat 2003)
% values for Aurélien's plexiglass sphere (elastic case)
	case 'plexi-full'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
% values for Aurélien's plexiglass sphere (elastic case) with 15cm-diameter inner polystyrene sphere
	case 'plexi-shell_15' 
		rf(1) = 0.15/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
% values for Ali's plexiglass sphere (elastic case) with 10cm-diameter inner polystyrene sphere
	case 'plexi-shell_10'
		rf(1) = 0.10/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
% values for Aurélien's plexiglass sphere (elastic case)
	case 'plexi-full.old'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.002 ; % shell thickness (m)
		rho(2) = 1800.; % shell density (kg/m^3)
		nu(2) = 0.4 ; % shell Poisson's ratio
		mu(2) = 2.9e9/2/(1+nu(2)); % shell shear modulus = E/2/(1+nu) (Pa)
% values for Aurélien's plexiglass sphere (elastic case) with 15cm-diameter inner polystyrene sphere
	case 'plexi-shell_15.old' 
		rf(1) = 0.15/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.002 ; % shell thickness (m)
		rho(2) = 1800.; % shell density (kg/m^3)
		nu(2) = 0.4 ; % shell Poisson's ratio
		mu(2) = 2.9e9/2/(1+nu(2)); % shell shear modulus = E/2/(1+nu) (Pa)
% values for Ali's plexiglass sphere (elastic case) with 10cm-diameter inner polystyrene sphere
	case 'plexi-shell_10.old'
		rf(1) = 0.10/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.002 ; % shell thickness (m)
		rho(2) = 1800.; % shell density (kg/m^3)
		nu(2) = 0.4 ; % shell Poisson's ratio
		mu(2) = 2.9e9/2/(1+nu(2)); % shell shear modulus = E/2/(1+nu) (Pa)
	case 'DTS-air'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.005 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
	case 'DTS-rigid-air'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = inf ; % shell thickness (m)
		rho(2) = 1.; % shell density (kg/m^3)
		nu(2) = 1.; % shell Poisson's ratio
		mu(2) = inf; % shell shear modulus (Pa)
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
	case 'DTS-sodium'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho_f = 980. ; % sodium density (kg/m^3)
		hat(2) = 0.005 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
		Gruneisen = 1.138; % Gruneisen parameter = C_P/C_V ?
		Prandtl = 0.01 ; % Prandtl number of sodium at 125°C
		nu_f = 6.67e-7; % (ms^-2) kinematic viscosity of sodium at 125°C
	case 'DTS-rigid-sodium'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho_f = 980. ; % sodium density (kg/m^3)
		hat(2) = Inf ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = Inf; % shell shear modulus (Pa)
		Gruneisen = 1.138; % Gruneisen parameter = C_P/C_V ?
		Prandtl = 0.01 ; % Prandtl number of sodium at 125°C
		nu_f = 6.67e-7; % (ms^-2) kinematic viscosity of sodium at 125°C
	case '30cm'
		rf(1) = 0.052; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.155; % internal radius 30cm sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.0127 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
	case '30cm-rigid-air'
		rf(1) = 0.052; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.155; % internal radius 30cm sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = Inf ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = Inf; % shell shear modulus (Pa)
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V of air at ??
		Prandtl = 0.713 ; % Prandtl number of air at 20°C
		nu_f = 15e-6; % (ms^-2) kinematic viscosity of air at 20°C
	case '30cm-water'
		rf(1) = 0.052; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.155; % internal radius 30cm sphere (m)
		csound = 1480.; % sound velocity in water (m/s)
%
		rho_f = 1000. ; % water density (kg/m^3)
		hat(2) = 0.0127 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
	case 'BigSister'
		rf(1) = 0.51; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1.46; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho_f = 980. ; % sodium density (kg/m^3)
% 304 stainless steel outer shell
		hat(2) = 0.025 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa
% 304 stainless steel inner shell
		hat(1) = 0.00635 ; % inner shell thickness (m) (from Santiago's mail, Feb 3, 2014)
		rho(1) = rho(2);
		nu(1) = nu(2);
		mu(1) = mu(2); 
		Gruneisen = 1.138; % Gruneisen parameter = C_P/C_V ?
		Prandtl = 0.01 ; % Prandtl number of sodium at 125°C
		nu_f = 6.67e-7; % (ms^-2) kinematic viscosity of sodium at 125°C
	case 'BigSister-rigid'
		rf(1) = 0.51; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1.46; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho_f = 980. ; % sodium density (kg/m^3)
% 304 stainless steel outer shell
		hat(2) = Inf ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = Inf; % shell shear modulus (Pa
% 304 stainless steel inner shell
		hat(1) = Inf ; % inner shell thickness (m) (from Santiago's mail, Feb 3, 2014)
		rho(1) = rho(2);
		nu(1) = nu(2);
		mu(1) = mu(2); 
		Gruneisen = 1.138; % Gruneisen parameter = C_P/C_V ?
		Prandtl = 0.01 ; % Prandtl number of sodium at 125°C
		nu_f = 6.67e-7; % (ms^-2) kinematic viscosity of sodium at 125°C
	otherwise
		disp(['Unknown case : ' sphere_type])
end
%
l_max = 15;
n_max = 5;
% ---------------------------------------------------------
% computation of the ideal elastic shell modes
% ---------------------------------------------------------
[kk, ff, BB, Acc] = elastic_spherical_shell_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max);
%
% ---------------------------------------------------------
% computation of the Ledoux coefficients (installed 160519)
% ---------------------------------------------------------
C_nl = Ledoux_coefficient(rf, kk, ff, BB);
draw_fl = input('draw Ledoux coefficients C_nl versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot Ledoux coefficients if wished
	if (~isempty(h_Ledoux_vs_l))
		figure(h_Ledoux_vs_l);
	else
		h_Ledoux_vs_l = figure('name','Ledoux coefficients','position',[100 100 600 800]);
	end
	if_compens = input('compensate with l factor (y/n) ? ','s');
	if (if_compens(1)=='y' || if_compens(1) == 'Y') % plot compensate by l(l+1) factor
		draw_Ledoux_coefficients_compens(n_max,l_max,C_nl,sphere_type);
	else
		draw_Ledoux_coefficients(n_max,l_max,C_nl,sphere_type);
	end
end
%
% ---------------------------------------------------------
% computation of the ellipticity coefficients (installed 170810)
% ---------------------------------------------------------
gamma_nl = ellipticity_coefficient(rf, kk, ff, BB);
draw_fl = input('draw ellipticity coefficients gamma_nl versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot ellipticity coefficients if wished
	if (~isempty(h_ellipticity_vs_l))
		figure(h_ellipticity_vs_l);
	else
		h_ellipticity_vs_l = figure('name','ellipticity coefficients','position',[100 100 600 800]);
	end
	if_compens = input('multiply by l^2 factor (y/n) ? ','s');
	if (if_compens(1)=='y' || if_compens(1) == 'Y') % plot compensate by l^2
		draw_ellipticity_coefficients_compens(n_max, l_max, gamma_nl, sphere_type);
	else
		draw_ellipticity_coefficients(n_max,l_max,gamma_nl,sphere_type);
	end
end
%
% ---------------------------------------------------------
% computation of the attenuation coefficients (installed 171016)
% ---------------------------------------------------------
[g_nl, delta_f_nl] = attenuation_half_width(rf, kk, ff, Gruneisen, Prandtl, nu_f);
draw_fl = input('draw attenuation half-widths g_nl versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot attenuation half-widths if wished
	if (~isempty(h_attenuation_vs_l))
		figure(h_attenuation_vs_l);
	else
		h_attenuation_vs_l = figure('name','attenuation coefficients','position',[100 100 600 800]);
	end
	draw_attenuation_coefficients(n_max,l_max,g_nl,sphere_type);
end

%
save_results = input(['Save results ? (y/n) ' char(10)],'s');
if (save_results == 'y' || save_results == 'Y')
	ReadMe_elastic_modes = [ ...
	'This file contains the results of the computation of acoustic modes in a spherical fluid shell ' char(10)...
	'enclosed between two solid shells (1=inner, 2=outer), which can be either rigid or elastic. ' char(10) ...
	'The elastic_spherical_shell_modes.m function used is based on the analysis ' ...
	'of Rand and DiMaggio (1967).' char(10) char(10) ...
	'The outputs are : ' char(10) ...
	'	kk(n+1,l+1) : the adimensional root k of mode nSl' char(10) ...
	'	ff(n+1,l+1) : the dimensional frequency f (Hz) of mode nSl' char(10) ...
	'	BB(n+1,l+1) : the adimensional ratio of the y over j Bessel function for mode nSl' char(10) ...
	'	C_nl(n+1,l+1) : the Ledoux coefficient for mode nSl' char(10) ...
	'	gamma_nl(n+1,l+1) : the ellipticity coefficient for mode nSl (see Dahlen, 1968)' char(10) ...
	'	Acc(n+1,l+1) : the dimensional Acceleration/Wall-pressure ratio (m/s^2/Pa) of the outer shell' char(10) ...
	'	g_nl(n+1,l+1) : the attenuation half-width for mode nSl (see Moldover et al, 1986)' char(10) ...
	'	delta_f_nl(n+1,l+1) : the frequency correction for mode nSl due to attenuation (see Moldover et al, 1986)' char(10) char(10) ...
	'Also stored : ' char(10) ...
	'	n_max : the maximum radial wave number n' char(10) ...
	'	l_max : the maximum angular wave number l' char(10) ...
	'	ReadMe_elastic_modes : the present message' char(10) ...
	'	parameters : a structure collecting the other input parameters : ' char(10) ...
	'		sphere_type, csound, rho_f, rf, hat, rho, mu, nu (see function for definitions).' char(10) ...
	'		Grunseisen, Prandtl, nu_f of the fluid for attenuation.' char(10) ...
	'		and ellipticity for a spheroid.' char(10) ...
	'NB : rf(1) = 0 means no inner sphere, and mu(i) = Inf means rigid "i" boundary.' char(10) ...
	'Note that the n order of modes is disrupted when boundaries are elastic : ' char(10) ...
	'you might want to re-order them using the modes computed for rigid boundaries as a reference.'
	];
	parameters.sphere_type = sphere_type;
	parameters.csound = csound;
	parameters.rho_f = rho_f;
	parameters.rf = rf;
	parameters.hat = hat;
	parameters.rho = rho;
	parameters.mu = mu;
	parameters.nu = nu;
	parameters.Gruneisen = Gruneisen;
	parameters.Prandtl = Prandtl;
	parameters.nu_f = nu_f;
	parameters.ellipticity = ellipticity;
%
	filename = sphere_type;
	write_acoustic_modes(octave,filename,parameters,n_max,l_max,kk,ff,BB, C_nl,Acc,gamma_nl,g_nl,delta_f_nl,ReadMe_elastic_modes);
end
