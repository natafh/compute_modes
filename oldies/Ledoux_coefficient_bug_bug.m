% -----------------------------------------------------------------------
function C_nl = Ledoux_coefficient(rf, kk, ff, BB);
% -----------------------------------------------------------------------
% computes the Ledoux coefficient C_nl (equation 3.361 of Asteroseismology by Aerts et al)
% that produces the splitting -m C_nl Omega of mode _nS_l^m due to the Coriolis force
% when the mode is observed in the rotating frame of the shell in solid body rotation Omega
%
% inputs :
%	rf(1) = radius of the inner sphere (0 if none) (m)
%	rf(2) = radius of the outer sphere (m)
%	kk(nn+1,ll+1) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	ff(nn+1,ll+1) = frequency of mode nnS_ll (Hz)
%	BB(nn+1,ll+1) = factor of the y_l part for mode nnS_ll
%
% Note : [n_max+1,l_max+1] = size(kk);
%
% outputs :
%	C_nl(nn+1,ll+1) = Ledoux coefficient
%
% 150903 HCN créé
%
[n_max, l_max] = size(kk);
n_max=n_max-1;
l_max=l_max-1;
%
for nn = 0:n_max
	disp(['n = ' int2str(nn)]);
	for ll = 0:l_max
		disp(['     l = ' int2str(ll)]);
		B = BB(nn+1,ll+1) ;
		k = kk(nn+1,ll+1) ;
% integrate function numerateur from rf(1) to rf(2)
		num = integral(@(r) numerateur(ll,k,B,r),rf(1),rf(2));
% integrate function denominateur from rf(1) to rf(2)
		denom = integral(@(r) denominateur(ll,k,B,r),rf(1),rf(2)) ;
		C_nl(nn+1,ll+1) = num/denom ;
	end % l loop
end % n loop
%
end
%% =================== fonctions pour integration ===================== %
% -----------------------------------------------------------------------
function f=zeta_r(l,k,B,r) 
% -----------------------------------------------------------------------
% radial displacement
f = d_spherical_bessel_j(l, k*r) + B * d_spherical_bessel_y(l, k*r) ;
end
% -----------------------------------------------------------------------
function f=zeta_h(l,k,B,r) 
% -----------------------------------------------------------------------
% horizontal displacement
f = (spherical_bessel_j(l, k*r) + B * spherical_bessel_y(l, k*r))./r ;
end
% -----------------------------------------------------------------------
function f=numerateur(l,k,B,r) 
% -----------------------------------------------------------------------
% integrand of the upper term of the C_nl Ledoux coefficient
f = (2*zeta_r(l,k,B,r).*zeta_h(l,k,B,r) + zeta_h(l,k,B,r).^2).*r.^2 ;
end
% -----------------------------------------------------------------------
function f=denominateur(l,k,B,r) 
% -----------------------------------------------------------------------
% integrand of the lower term of the C_nl Ledoux coefficient
f = (zeta_r(l,k,B,r).^2 + l*(l+1)*zeta_h(l,k,B,r).^2).*r.^2 ;
end
%% ================== fonctions de Bessel sphériques =================== %
% -----------------------------------
function f=d_spherical_bessel_j(n, x)
% -----------------------------------
% d/dx j(n,x)
	f = sqrt(1/2/pi)*(-besselj(n+1/2,x).*(x.^(-3/2)) + (besselj(n-1/2,x)-besselj(n+3/2,x)).*(x.^(-1/2)));
end
% -----------------------------------
function f=d_spherical_bessel_y(n, x)
% -----------------------------------
% d/dx y(n,x)
	f = sqrt(1/2/pi)*(-bessely(n+1/2,x).*(x.^(-3/2)) + (bessely(n-1/2,x)-bessely(n+3/2,x)).*(x.^(-1/2)));
end
% ---------------------------------
function f=spherical_bessel_j(n, x)
% ---------------------------------
% j(n,x) = sqrt(2/pi x) * J(n + 1/2, x)
	f = sqrt(pi/2)*besselj(n+1/2,x)./sqrt(x);
end
% ---------------------------------
function f=spherical_bessel_y(n, x)
% ---------------------------------
% y(n,x) = sqrt(2/pi x) * Y(n + 1/2, x)
	f = sqrt(pi/2)*bessely(n+1/2,x)./sqrt(x);
end
%% ================== =============================== =================== %
