% -----------------------------------------------------------------------
function gamma_nl = ellipticity_coefficient(rf, kk, ff, BB);
% -----------------------------------------------------------------------
% computes the ellipticity coefficient gamma_nl
% (adapté de l'Appendice A de Dahlen (1968))
% that produces the splitting relatif (-l(l+1)/3 + m^2) gamma_nl ellipticity
% of mode _nS_l^m due to the ellipticity of the outer boundary (radius r_o),
% defined by: r[1 - 2/3 ellipticity P_2^0(cos theta)] = r_o.
%
% guessed for rigid boundary.
% *** Should not apply directly to elastic outer boundary ***
%
% 170809 HCN *** en cours ***

%
% inputs :
%	rf(1) = radius of the inner sphere (0 if none) (m)
%	rf(2) = radius of the outer sphere (m)
%	kk(nn+1,ll+1) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	ff(nn+1,ll+1) = frequency of mode nnS_ll (Hz)
%	BB(nn+1,ll+1) = factor of the y_l part for mode nnS_ll
%
% Note : [n_max+1,l_max+1] = size(kk);
%
% outputs :
%	gamma_nl(nn+1,ll+1) = ellipticity coefficient
%
[n_max, l_max] = size(kk);
n_max=n_max-1;
l_max=l_max-1;
%
for nn = 0:n_max
	disp(['n = ' int2str(nn)]);
	for ll = 0:l_max
		disp(['     l = ' int2str(ll)]);
		B = BB(nn+1,ll+1) ;
		k = kk(nn+1,ll+1) ;
% upper term of the gamma_nl ellipticity coefficient
% (it is the jump condition between the fluid and the shell
% at the (surface : r=1) elliptical boundary)
% NB : *** it should not apply to an elastic shell ***
		num = numerateur(ll,k,B,1); % (pas d'intégration : valeur en r=1)
% integrate function denominateur from rf(1) to rf(2) (adim)
		denom = integral(@(r) denominateur(ll,k,B,r),rf(1)/rf(2),1) ;
		gamma_nl(nn+1,ll+1) = num/denom/(2*ll-1)/(2*ll+3) ;
	end % l loop
end % n loop
%
end
%% =================== fonctions pour integration ===================== %
% -----------------------------------------------------------------------
function f=zeta_r(l,k,B,r) 
% -----------------------------------------------------------------------
% radial displacement
f = d_spherical_bessel_j(l, k*r) + B * d_spherical_bessel_y(l, k*r) ;
end
% -----------------------------------------------------------------------
function f=zeta_h(l,k,B,r) 
% -----------------------------------------------------------------------
% horizontal displacement
f = (spherical_bessel_j(l, k*r) + B * spherical_bessel_y(l, k*r))./r ;
end
% -----------------------------------------------------------------------
function f=d_zeta_r(l,k,B,r) 
% -----------------------------------------------------------------------
% radial derivative of the radial displacement
f = d2_spherical_bessel_j(l, k*r) + B * d2_spherical_bessel_y(l, k*r) ;
end
% -----------------------------------------------------------------------
function f=numerateur(l,k,B,r) 
% -----------------------------------------------------------------------
% upper term of the gamma_nl ellipticity coefficient
% (it is the jump condition between the fluid and the shell
% at the (surface : r=1) elliptical boundary)
f = ((1./k)*(d_zeta_r(l,k,B,r) + (1./r).*(2*zeta_r(l,k,B,r) ...
                                  - l*(l+1)*zeta_h(l,k,B,r)))).^2 ...
- (zeta_r(l,k,B,r).^2 + (l*(l+1)-3)*zeta_h(l,k,B,r).^2) ;
end
% -----------------------------------------------------------------------
function f=denominateur(l,k,B,r) 
% -----------------------------------------------------------------------
% integrand of the lower term of the gamma_nl ellipticity coefficient
% (the same as for Ledoux coefficients)
f = (zeta_r(l,k,B,r).^2 + l*(l+1)*zeta_h(l,k,B,r).^2).*r.^2 ;
end
%% ================== fonctions de Bessel sphériques =================== %
% -----------------------------------
function f=d2_spherical_bessel_j(n, x)
% -----------------------------------
% d2/dx2 j(n,x)
% 170810 HCN
	f = sqrt(pi./(2*x.^3)).*(besselj(n+1/2,x).*(n*(n-1)./x - x) + 2*besselj(n+3/2,x));
end
% -----------------------------------
function f=d2_spherical_bessel_y(n, x)
% -----------------------------------
% d2/dx2 y(n,x)
% 170810 HCN
	f = sqrt(pi./(2*x.^3)).*(bessely(n+1/2,x).*(n*(n-1)./x - x) + 2*bessely(n+3/2,x));
end
% -----------------------------------
function f=d_spherical_bessel_j(n, x)
% -----------------------------------
% d/dx j(n,x)
% 170810 HCN : factor pi/2 bug corrected + more compact
	f = sqrt(pi./(2*x)).*(n*besselj(n+1/2,x)./x - besselj(n+3/2,x));
end
% -----------------------------------
function f=d_spherical_bessel_y(n, x)
% -----------------------------------
% d/dx y(n,x)
% 170810 HCN : factor pi/2 bug corrected + more compact
	f = sqrt(pi./(2*x)).*(n*bessely(n+1/2,x)./x - bessely(n+3/2,x));
end
% ---------------------------------
function f=spherical_bessel_j(n, x)
% ---------------------------------
% j(n,x) = sqrt(2/pi x) * J(n + 1/2, x)
	f = sqrt(pi/2)*besselj(n+1/2,x)./sqrt(x);
end
% ---------------------------------
function f=spherical_bessel_y(n, x)
% ---------------------------------
% y(n,x) = sqrt(2/pi x) * Y(n + 1/2, x)
	f = sqrt(pi/2)*bessely(n+1/2,x)./sqrt(x);
end
%% ================== =============================== =================== %
