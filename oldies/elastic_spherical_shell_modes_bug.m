% -----------------------------------------------------------------------
function [kk, ff, BB, Acc] = elastic_spherical_shell_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max)
% -----------------------------------------------------------------------
% computes the 'k' kk, frequency ff and B coefft of acoustic modes (nn,ll)
% in a spherical shell of fluid
% enclosed between an inner solid shell and an outer solid shell.
% Both the inner and the outer shells can be either rigid or elastic
% Seismological notation used (ex: 0S13, 1S0, etc)
% note : ff = kk * csound/(2 pi r_o)
% 140124 : adapted from paper by Rand and DiMaggio (1967), following Aurélien modes_sphere matlab program
% the pressure in the fluid is expressed as :
%	P(r,cos(theta)) = P_l(cos(theta)) * [j_l(kr) + B y_l(kr)]
% where j_l and y_l are the spherical Bessel functions of the first and second kind
% we define the 'j_term' and 'y_term' at the boundaries to get the frequency equation
% 
% inputs :
%	csound = sound velocity (m/s)
%	rho_f = density of the fluid (kgm^-3)
%	rf(1) = radius of the inner sphere (0 if none) (m)
%	rf(2) = radius of the outer sphere (m)
%	hat(1) = thickness of the inner elastic shell (m)
%	hat(2) = thickness of the outer elastic shell (m)
%	mu(1) = shear modulus of the inner elastic shell (Pa) (Inf if rigid)
%	mu(2) = shear modulus of the outer elastic shell (Pa) (Inf if rigid)
%	nu(1) = Poisson's ratio of the inner elastic shell
%	nu(2) = Poisson's ratio of the outer elastic shell
%	rho(1) = density of the inner elastic shell (kgm^-3)
%	rho(2) = density of the outer elastic shell (kgm^-3)
%	n_max = max radial mode number (0 to n_max)
%	l_max = max angular mode number (0 to l_max)
%
% outputs :
%	kk(nn+1,ll+1) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	ff(nn+1,ll+1) = frequency of mode nnS_ll (Hz)
%	BB(nn+1,ll+1) = factor of the y_l part for mode nnS_ll
%	Acc(nn+1,ll+1) = Acceleration/Wall Pressure ratio for mode nnS_ll (m/s^2/Pa)
% Note : Acc is independent of rho_f although it does not appear like so
%
global h_f_vs_l % declare figure handle h_f_vs_l as global

	disp('Welcome in elastic_spherical_shell_modes, which computes the acoustic modes in a spherical shell of fluid enclosed between a solid inner shell and a solid outer shell. Shells are either rigid or elastic.')	    
% derived relevant parameters
lambda = rho * csound^2 ./ mu / 2.; % lambda(1:2) parameter of Rand and DiMaggio
kappa_0 = (rho_f./rho) .* (rf./hat); % kappa_0(1:2) parameter of Rand and DiMaggio

if (rf(1) == 0) % no inner sphere
% full fluid sphere with rigid or elastic shell
	f_sphere = @(l,x) j_term(l,x,lambda(2),kappa_0(2),nu(2));
	kk = f_modes(f_sphere, n_max, l_max); % 'k' of modes (0:n_max,0:l_max)
	BB = zeros(n_max+1,l_max+1); % no y_l term
%
	for ll=0:l_max
		l = ll+1;
		clear z
		z(1:n_max+1) = kk(1:n_max+1,l); % the computed k (or z) of all modes with ll angular number
% Acceleration/Wall pressure ratio from equation (15) of Rand and DiMaggio (1967)
		Acc(1:n_max+1,l) = -(z(1:n_max+1)/rho_f/rf(2)) .*(d_spherical_bessel_j(ll, z(1:n_max+1)))./(spherical_bessel_j(ll, z(1:n_max+1)));
	end
else % there is an inner sphere
	f_sphere = @(l,x) (j_term(l,x,lambda(2),kappa_0(2),nu(2)) * y_term(l,x*rf(1)/rf(2),lambda(1),kappa_0(1),nu(1)) - y_term(l,x,lambda(2),kappa_0(2),nu(2)) * j_term(l,x*rf(1)/rf(2),lambda(1),kappa_0(1),nu(1)));
	kk = f_modes(f_sphere, n_max, l_max); % 'k' of modes (0:n_max,0:l_max)
%
	for ll=0:l_max
		l = ll+1;
		clear z
		z(1:n_max+1) = kk(1:n_max+1,l); % the computed k (or z) of all modes with ll angular number
%
% Amplitude BB of the y_l term (relative to the j_l term) from outer boundary condition
		BB(1:n_max+1,l) = -j_term(ll,z(1:n_max+1),lambda(2),kappa_0(2),nu(2))./y_term(ll,z(1:n_max+1),lambda(2),kappa_0(2),nu(2));
		%
% Acceleration/Wall pressure ratio from equation (15) of Rand and DiMaggio (1967)
% NB : need to transpose BB matrix for .product
		Acc(1:n_max+1,l) = -(z(1:n_max+1)/rho_f/rf(2)) .*(d_spherical_bessel_j(ll, z(1:n_max+1)) + BB(1:n_max+1,l)'.*d_spherical_bessel_y(ll, z(1:n_max+1)))./(spherical_bessel_j(ll, z(1:n_max+1)) + BB(1:n_max+1,l)'.*spherical_bessel_y(ll, z(1:n_max+1)));
	end
end

ff = kk*csound/(2*pi*rf(2)); % frequency of modes (nn,ll)
	
draw_fl = input('draw frequency versus l figure (y/n) ? ','s');
if (draw_fl(1)=='y' || draw_fl(1) == 'Y') % plot mode frequencies if wished
	if (~isempty(h_f_vs_l))
		figure(h_f_vs_l);
	else
		h_f_vs_l = figure('name','mode frequencies','position',[100 100 600 800]);
	end
	draw_elastic_modes(n_max,l_max,ff,csound,rf,lambda,kappa_0,nu);
end
%
end % end function

% ---------------------------------
function z = f_modes(f, nmax, lmax)
% ---------------------------------
	z = zeros(nmax+1,lmax+1);	
	for l = 0:lmax
		z(:,l+1) = fzeros(@(x) f(l,x), l+1, Inf, nmax+1, 1);
	end	    
% correction pour le mode (0,0)
	z(:,1)=[0; z(1:end-1,1)];
end

% -------------------------------------------
function z = fzeros(f, x0, xmax, nmax, delta)
% -------------------------------------------
%% fzeros : résolution de f(x) = 0
% f = fonction dont on cherche les zéros
% x0 = point de départ (début intervalle : doit être inférieur à tous les
% zéros recherchés)
% xmax = fin de l'intervalle de recherche (peut être +Inf)
% nmax = nombre de zéros recherchés (peut être +Inf)
% delta = écart minimal entre les zéros
%
	threshold = 1;   % limite déterminant si deux valeurs trouvées sont égales
	alloc_step = 128;   % si le nombre de zéros n'est pas spécifié, on alloue un tableau par tranches arbitraires
    
	if nmax < Inf
		z = zeros(nmax,1);
	else
		z = zeros(alloc_step,1);
	end

	n = 1;
	x = x0;
	while x < xmax && n <= nmax
% recherche le zéro le plus proche
		[xn, ~, err] = fzero(f, x, optimset('Display','off'));
%       [xn, ~, err] = fsolve(f, x); % 130829 HCN : replace fzero by fsolve
        
% vérifie si on a trouvé une nouvelle valeur
	if err == 1 && (n == 1 || abs(xn - z(n - 1)) > threshold)
		z(n) = xn;
		n = n + 1;
% augmente la taille du tableau si nécessaire
			if nmax == Inf && mod(n, alloc_step) == 0
				z = [z ; zeros(alloc_step, 1)];
			end
			x = xn;
		end
		x = x + delta;
	end
	z(n:end) = [];
end

%% ================== fonction for elastic shell =================== %
% -----------------------------------
function f = elastic_spherical_shell(ll,z,lambda,kappa_0,nu);
% -----------------------------------
% equation (21) of Rand and DiMaggio (ll is the angular mode number)
% *** obsolete (for check) ***
	crochet1 = (1-nu)/(1+nu)*lambda*z.^2 - 2;
	accolade = (crochet1*(1 + lambda*z.^2) + ll*(ll+1)*(1 - lambda*z.^2/(1+nu)));
	crochet2 = ll - z*besselj(ll+3/2,z)./besselj(ll+1/2,z);
	crochet3 = (1-nu)/(1+nu)*(1 + lambda*z.^2) - ll*(ll+1)/(1+nu);
	f = accolade*crochet2 + kappa_0*lambda*z.^2*crochet3;
end
% -----------------------------------
function f = j_term(ll,z,lambda,kappa_0,nu);
% -----------------------------------
% elastic case from equation (18) of Rand and DiMaggio (1967)
% with g123 combining the g1, g2 and g3 given in their Appendix (eqns A25-A27)
% and g456 combining the g4, g5 and g6 given in their Appendix (eqns A28-A30)
% NB : z can be a vector
%
	if (lambda == 0) % rigid shell
		f = d_spherical_bessel_j(ll,z);
	else % elastic shell
		g123 = (1 + lambda*z.^2) .* (2 - (1-nu)/(1+nu)*lambda*z.^2) - ll*(ll+1)*(1 - lambda*z.^2/(1+nu));
		g456 = lambda*kappa_0*z./(1+nu) .* (ll*(ll+1) - (1-nu)*(1+lambda*z.^2));
		f = g123 .* d_spherical_bessel_j(ll,z) + g456 .* spherical_bessel_j(ll,z);
	end
end
% -----------------------------------
function f = y_term(ll,z,lambda,kappa_0,nu);
% -----------------------------------
% elastic case from equation (18) of Rand and DiMaggio (1967)
% with g123 combining the g1, g2 and g3 given in their Appendix (eqns A25-A27)
% and g456 combining the g4, g5 and g6 given in their Appendix (eqns A28-A30)
% The y_term is added to deal with an inner sphere
% NB : z can be a vector

	if (lambda == 0) % rigid shell
		f = d_spherical_bessel_y(ll,z);
	else
		g123 = (1 + lambda*z.^2) .* (2 - (1-nu)/(1+nu)*lambda*z.^2) - ll*(ll+1)*(1 - lambda*z.^2/(1+nu));
		g456 = lambda*kappa_0*z./(1+nu) .* (ll*(ll+1) - (1-nu)*(1+lambda*z.^2));
		f = g123 .* d_spherical_bessel_y(ll,z) + g456 .* spherical_bessel_y(ll,z);
	end
end
%% ================== fonctions de Bessel sphériques =================== %
% -----------------------------------
function f=d_spherical_bessel_j(n, x)
% -----------------------------------
% d/dx j(n,x)
	f = sqrt(1/2/pi)*(-besselj(n+1/2,x).*(x.^(-3/2)) + (besselj(n-1/2,x)-besselj(n+3/2,x)).*(x.^(-1/2)));
end
% -----------------------------------
function f=d_spherical_bessel_y(n, x)
% -----------------------------------
% d/dx y(n,x)
	f = sqrt(1/2/pi)*(-bessely(n+1/2,x).*(x.^(-3/2)) + (bessely(n-1/2,x)-bessely(n+3/2,x)).*(x.^(-1/2)));
end
% ---------------------------------
function f=spherical_bessel_j(n, x)
% ---------------------------------
% j(n,x) = sqrt(2/pi x) * J(n + 1/2, x)
	f = sqrt(pi/2)*besselj(n+1/2,x)./sqrt(x);
end
% ---------------------------------
function f=spherical_bessel_y(n, x)
% ---------------------------------
% y(n,x) = sqrt(2/pi x) * Y(n + 1/2, x)
	f = sqrt(pi/2)*bessely(n+1/2,x)./sqrt(x);
end
%% ================== =============================== =================== %