%
sphere_type = input(['Enter sphere type (plexi, DTS-air, DTS-sodium, 30cm, BigSister, )' char(10)],'s');
%
r_i = 0.;
switch sphere_type
% values for Aurélien's plexiglass sphere (elastic case)
	case 'plexi'
		r_i = 0.15/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		r_o = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho = 1.18 ; % air density (kg/m^3)
		h_s = 0.002 ; % shell thickness (m)
		rho_s = 1800.; % shell density (kg/m^3)
		nu = 0.4 ; % shell Poisson's ratio
		mu = 2.9e9/2/(1+nu); % shell shear modulus = E/2/(1+nu) (Pa)
	case 'DTS-air'
		r_i = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		r_o = 0.210; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho = 1.18 ; % air density (kg/m^3)
		h_s = 0.005 ; % shell thickness (m)
		rho_s = 7990.; % shell density (kg/m^3)
		nu = 0.3 ; % shell Poisson's ratio
		mu = 1.8e11/2/(1+nu); % shell shear modulus (Pa)
	case 'DTS-sodium'
		r_i = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		r_o = 0.210; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho = 980. ; % sodium density (kg/m^3)
		h_s = 0.005 ; % shell thickness (m)
		rho_s = 7990.; % shell density (kg/m^3)
		nu = 0.3 ; % shell Poisson's ratio
		mu = 1.8e11/2/(1+nu); % shell shear modulus (Pa)
	case '30cm'
		r_i = 0.102/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		r_o = 0.15; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho = 1.18 ; % air density (kg/m^3)
		h_s = 0.0127 ; % shell thickness (m)
		rho_s = 7990.; % shell density (kg/m^3)
		nu = 0.3 ; % shell Poisson's ratio
		mu = 1.8e11/2/(1+nu); % shell shear modulus (Pa)
	case 'BigSister'
		r_i = 0.51; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		r_o = 1.46; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho = 980. ; % sodium density (kg/m^3)
		h_s = 0.025 ; % shell thickness (m)
		rho_s = 7990.; % shell density (kg/m^3)
		nu = 0.3 ; % shell Poisson's ratio
		mu = 1.8e11/2/(1+nu); % shell shear modulus (Pa)
	otherwise
		disp(['Unknown case ' sphere_type])
end
%
l_max = 16;
n_max = 6;
%
% [kk, ff] = spherical_elastic_shell_modes(r_o, h_s, rho, rho_s, csound, mu, nu, n_max, l_max);
[kk, ff] = spherical_elastic_shell_with_inner_modes(r_i, r_o, h_s, rho, rho_s, csound, mu, nu, n_max, l_max);