% -----------------------------------------------------------
% Program to compute the acoustic modes of our vaious set-ups
% -----------------------------------------------------------
% 140203 HCN : added properties for BigSister
% 140124 HCN
% calls function elastic_spherical_shell _modes, which can deal with elastic
% or rigid boundaries, with or without an inner sphere
% set mu(i) and/or hat(i) to make i-shell rigid
% set rf(1) = 0 to remove the inner sphere 
%
global h_f_vs_l % declare figure handle h_f_vs_l as global

sphere_type = input(['Enter sphere type (plexi-full, plexi-full-test, plexi-shell_10, plexi-shell_15, DTS-air, DTS-sodium, 30cm, BigSister, )' char(10)],'s');
%
rf(1) = 0.; % no inner sphere
mu(1) = Inf; % rigid inner sphere
hat(1) = Inf; % dumy
rho(1) = 1.; % dumy
nu(1) = 1.; % dumy
%
switch sphere_type
% values for Aurélien's plexiglass sphere (elastic case)
	case 'plexi-full-test'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = Inf; % shell shear modulus (Pa) (Idemat 2003)
% values for Aurélien's plexiglass sphere (elastic case)
	case 'plexi-full'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
% values for Aurélien's plexiglass sphere (elastic case) with 15cm-diameter inner polystyrene sphere
	case 'plexi-shell_15' 
		rf(1) = 0.15/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
% values for Ali's plexiglass sphere (elastic case) with 10cm-diameter inner polystyrene sphere
	case 'plexi-shell_10'
		rf(1) = 0.10/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.222; % internal radius plexi sphere (m) (Ali's new estimate 140904)
		csound = 347.5; % sound velocity in air @ 27.4°C (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.00236 ; % shell thickness (m) (Ali's new estimate)
		rho(2) = 1190.; % shell density (kg/m^3) (Idemat 2003)
		nu(2) = 0.39 ; % shell Poisson's ratio (Johnstone)
		mu(2) = 1.7e9; % shell shear modulus (Pa) (Idemat 2003)
% values for Aurélien's plexiglass sphere (elastic case)
	case 'plexi-full.old'
		rf(1) = 0; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.002 ; % shell thickness (m)
		rho(2) = 1800.; % shell density (kg/m^3)
		nu(2) = 0.4 ; % shell Poisson's ratio
		mu(2) = 2.9e9/2/(1+nu(2)); % shell shear modulus = E/2/(1+nu) (Pa)
% values for Aurélien's plexiglass sphere (elastic case) with 15cm-diameter inner polystyrene sphere
	case 'plexi-shell_15.old' 
		rf(1) = 0.15/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.002 ; % shell thickness (m)
		rho(2) = 1800.; % shell density (kg/m^3)
		nu(2) = 0.4 ; % shell Poisson's ratio
		mu(2) = 2.9e9/2/(1+nu(2)); % shell shear modulus = E/2/(1+nu) (Pa)
% values for Ali's plexiglass sphere (elastic case) with 10cm-diameter inner polystyrene sphere
	case 'plexi-shell_10.old'
		rf(1) = 0.10/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.446/2; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.002 ; % shell thickness (m)
		rho(2) = 1800.; % shell density (kg/m^3)
		nu(2) = 0.4 ; % shell Poisson's ratio
		mu(2) = 2.9e9/2/(1+nu(2)); % shell shear modulus = E/2/(1+nu) (Pa)
	case 'DTS-air'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius plexi sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.005 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
	case 'DTS-sodium'
		rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.210; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho_f = 980. ; % sodium density (kg/m^3)
		hat(2) = 0.005 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
	case '30cm'
		rf(1) = 0.102/2; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 0.15; % internal radius 30cm sphere (m)
		csound = 340.; % sound velocity in air (m/s)
%
		rho_f = 1.18 ; % air density (kg/m^3)
		hat(2) = 0.0127 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
	case 'BigSister'
		rf(1) = 0.51; % radius of the inner sphere (m) (set to 0 if no inner sphere)
		rf(2) = 1.46; % internal radius plexi sphere (m)
		csound = 2500.; % sound velocity in sodium (m/s)
%
		rho_f = 980. ; % sodium density (kg/m^3)
% 304 stainless steel outer shell
		hat(2) = 0.025 ; % shell thickness (m)
		rho(2) = 7990.; % shell density (kg/m^3)
		nu(2) = 0.3 ; % shell Poisson's ratio
		mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa
% 304 stainless steel inner shell
		hat(1) = 0.00635 ; % inner shell thickness (m) (from Santiago's mail, Feb 3, 2014)
		rho(1) = rho(2);
		nu(1) = nu(2);
		mu(1) = mu(2); 
	otherwise
		disp(['Unknown case : ' sphere_type])
end
%
l_max = 15;
n_max = 5;
%
[kk, ff, BB, Acc] = elastic_spherical_shell_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max);

save_results = input(['Save results ? (y/n) ' char(10)],'s');
if (save_results == 'y' || save_results == 'Y')
	ReadMe_elastic_modes = [ ...
	'This file contains the results of the computation of acoustic modes in a spherical fluid shell ' char(10)...
	'enclosed between two solid shells (1=inner, 2=outer), which can be either rigid or elastic. ' char(10) ...
	'The elastic_spherical_shell_modes.m function used is based on the analysis ' ...
	'of Rand and DiMaggio (1967).' char(10) char(10) ...
	'The outputs are : ' char(10) ...
	'	kk(n+1,l+1) : the adimensional root k of mode nSl' char(10) ...
	'	ff(n+1,l+1) : the dimensional frequency f (Hz) of mode nSl' char(10) ...
	'	BB(n+1,l+1) : the adimensional ratio of the y over j Bessel function for mode nSl' char(10) ...
	'	Acc(n+1,l+1) : the dimensional Acceleration/Wall-pressure ratio (m/s^2/Pa) of the outer shell' char(10) char(10) ...
	'Also stored : ' char(10) ...
	'	n_max : the maximum radial wave number n' char(10) ...
	'	l_max : the maximum angular wave number l' char(10) ...
	'	ReadMe_elastic_modes : the present message' char(10) ...
	'	parameters : a structure collecting the other input parameters : ' char(10) ...
	'		sphere_type, csound, rho_f, rf, hat, rho, mu, nu (see function for definitions).' char(10) ...
	'NB : rf(1) = 0 means no inner sphere, and mu(i) = Inf means rigid "i" boundary.' char(10) ...
	'Note that the n order of modes is disrupted when boundaries are elastic : ' char(10) ...
	'you might want to re-order them using the modes computed for rigid boundaries as a reference.'
	];
	parameters.sphere_type = sphere_type;
	parameters.csound = csound;
	parameters.rho_f = rho_f;
	parameters.rf = rf;
	parameters.hat = hat;
	parameters.rho = rho;
	parameters.mu = mu;
	parameters.nu = nu;
	filename = 'elastic_modes.mat';
	save(filename,'n_max', 'l_max', 'kk','ff','BB','Acc','ReadMe_elastic_modes','parameters')
	disp(['Results saved in : ' filename])
end
