% ---------------------------------------------------------------------
function draw_Ledoux_coefficients_compens(h_fig, n_max, l_max, C_nl, sphere_type)
% ---------------------------------------------------------------------
% 190118 HCN change figure handling
% 160519 HCN from draw_mode_frequencies
% function to draw the Ledoux coefficients in elastic or rigid shells
% 170710 : variante pour compenser d'un facteur (l+1)
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
title1 = 'C_{nl} Ledoux coefficients x (l+1) for modes _nS_l';
title2 = 'in a spherical shell of fluid enclosed between inner and outer shells';
title3 = ['sphere type : ' sphere_type '       (' date ')'];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['(l+1) x Ledoux coefficient'])
box on
grid on
hold on
% multiply by (l+1) to see compensation effect
compens = (1:l_max+1); % (l+1)
%compens = compens.^2; % (l+1)^2
size(compens)
%
for nn=n_max:-1:0
	nnn=nn+1;
	to_legend = int2str(nn);
	if (nn == n_max)
		to_legend = ['n=' to_legend];
	end
	plot((1:l_max+1)-1,C_nl(nnn,1:l_max+1).*compens,[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn),'DisplayName',to_legend)
end
legend('show')
