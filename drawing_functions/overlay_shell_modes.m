% ------------------------------------------------------
function overlay_shell_modes(h_fig,n_max,l_max,ff_shell)
% ------------------------------------------------------
% 190625 HCN copied from draw_mode_frequencies
% function to overlay the frequencies of the modes of a spherical elastic shell
% on the f versus l figure of acoustic modes
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
for nn=n_max:-1:0
	nnn=nn+1;
	to_legend = int2str(nn);
	if (nn==n_max)
	   to_legend = ['n=' to_legend];
	end
	plot((1:l_max+1)-1,ff_shell(nnn,1:l_max+1),[coul(nnn) marker(nnn)], 'MarkerSize',10,'DisplayName',to_legend)
end
%
legend('show')
