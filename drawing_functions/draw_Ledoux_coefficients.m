% ---------------------------------------------------------------------
function draw_Ledoux_coefficients(h_fig,n_max,l_max,C_nl,sphere_type)
% ---------------------------------------------------------------------
% 190118 HCN change figure handling
% 160519 HCN from draw_mode_frequencies
% function to draw the Ledoux coefficients in elastic or rigid shells
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
title1 = 'C_{nl} Ledoux coefficients for modes _nS_l';
title2 = 'in a spherical shell of fluid enclosed between inner and outer shells';
title3 = ['sphere type : ' sphere_type '     (' date ')'];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['Ledoux coefficient'])
box on
grid on
%
for nn=n_max:-1:0
	nnn=nn+1;
	plot((1:l_max+1)-1,C_nl(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn))
	to_legend = int2str(nn);
	if (nn == n_max)
		legend_cell={['n=' to_legend]};
	else
		legend_cell=[legend_cell to_legend];
	end
end
legend(legend_cell)
