% Program to draw the combined effects of ellipticity and global rotation
% on the splitting of modes in ZoRo^2, from data in modes file
%
% 170927 HCN
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'compute_modes'])
%
marker = 'ox+vsphox+vsphox+vsphox+vsph';
couleur = 'kbrmgkbrmgkbrmgkbrmgkbrmgkbrmg';

%
fprintf(['Welcome in draw_elliptic_rotation_splitting !' char(10)])
filename = input(['enter the sphere type of the modes file you want to plot (ex: ZoRo-rigid-air)' char(10)],'s');
%

% read modes file and associated data
[parameters,n_max,l_max, kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, ReadMe_elastic_modes] = read_acoustic_modes(filename);
l_tot = l_max+1;
n_tot = n_max+1;
% builds name of modes
for nn=0:n_max
	for ll=0:l_max
		name_ = ['_' int2str(nn) 'S_' int2str(ll)]; % ex: _0S_4
		name{nn+1,ll+1} = name_; % array of cells
	end
end
%
ellipticity = input(['enter the relative ellipticity (ex: 0.1) ']);
Omega = input(['enter the global rotation rate in Hz (ex: 25) ']);
% Rossby = 0.01; % typical Rossby number of the convective jets (guess...)
%
figure('Name','ellipticity and global rotation mode splitting')
%	stem(ff(ntn,1:l_max), ((1:l_max)*0+3-ntn/5)*1000,[marker(ntn) coul(ntn)],'LineStyle',':');
%
% ------------------------------------------------
% original degenerate SNRI modes (with their name)
% ------------------------------------------------
subplot(3,1,1)
for nn=0:n_max
	nnn=nn+1;
	stem(ff(nnn,1:l_tot),ones(1,l_tot).*10+nn,[marker(nnn) 'k'],'DisplayName',['n=' int2str(nn)]);
	hold on
	text(ff(nnn,1:l_tot),ones(1,l_tot).*11+nn,{name{nnn,1:l_tot}}, 'HorizontalAlignment', 'center') % note syntax for multiple text
end
%
xlabel('mode frequency (Hz)')
ylabel('fake amplitude')
legend('location','best')
xlim([300 5000])
title(['degenerate SNRI multiplets for ' filename])
%
% --------------------------------------------
% multiplets with splitting due to ellipticity
% --------------------------------------------
subplot(3,1,2)
for nn=0:n_max
	nnn=nn+1;
	for ll=0:l_max
		lll=ll+1;
		for mm=0:ll
			mmm=mm+1;
			yy = 10.-mm*0.5;
			xx = ff(nnn,lll) * (1 - ellipticity * (-ll*(ll+1)/3. + mm^2) * gamma_nl(nnn,lll));
			stem(xx,yy,[marker(nnn) couleur(mmm)])
			hold on
		end
	end
end
xlabel('mode frequency (Hz)')
ylabel('fake amplitude')
xlim([300 5000])
title(['ellipticity-split singlets for \epsilon = ' num2str(ellipticity)])
% small color (and height) legend of the m (to the right of f max)
truc=xlim;
for mm=0:l_max
	mmm=mm+1;
	text(truc(2)+10,10-mm*0.5,['m=' int2str(mm)],'Color',couleur(mmm))
end
%
% -------------------------------------------------------------------------------
% multiplets with splitting due to ellipticity + splitting due to global rotation
% -------------------------------------------------------------------------------
subplot(3,1,3)
for nn=0:n_max
	nnn=nn+1;
	for ll=0:l_max
		lll=ll+1;
		for mm=0:ll
			mmm=mm+1;
			yy(1:2) = 10.-mm*0.5;
			xx0 = ff(nnn,lll) * (1 - ellipticity * (-ll*(ll+1)/3. + mm^2) * gamma_nl(nnn,lll));
			xx(1) = xx0 - mm*C_nl(nnn,lll)*Omega;
			xx(2) = xx0 + mm*C_nl(nnn,lll)*Omega;
			stem(xx,yy,[marker(nnn) couleur(mmm)])
			hold on
		end
	end
end
xlabel('mode frequency (Hz)')
ylabel('fake amplitude')
xlim([300 5000])
title(['ellipticity and global rotation-split singlets for f_o = ' num2str(Omega) ' Hz'])
