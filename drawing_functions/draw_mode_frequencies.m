% ---------------------------------------------------------------------
function draw_mode_frequencies(h_fig,n_max,l_max,ff,csound,full_name)
% ---------------------------------------------------------------------
% 190118 HCN title modified
% 140117 HCN
% function to draw the frequencies of acoustic modes in elastic or rigid shells
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
for nn=n_max:-1:0
	nnn=nn+1;
%if (superpose == 0)
    to_legend = int2str(nn);
    if (nn==n_max)
        to_legend = ['n=' to_legend];
    end
	plot((1:l_max+1)-1,ff(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor', coul(nnn),'DisplayName',to_legend)
%else
%	plot((1:l_max+1)-1,ff(nnn,1:l_max+1),[coul(nnn) marker(nnn)])
%end
end
%
title1 = '_nS_l mode frequencies for elastic or rigid spherical shell';
title([title1 char(10) full_name]);
xlabel('mode angular number l')
ylabel(['mode frequency (Hz) (csound = ' num2str(csound) ' m/s)'])
box on
grid on
legend('show')
