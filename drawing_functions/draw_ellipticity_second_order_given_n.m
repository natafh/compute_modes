% ---------------------------------------------------------------------------------------
function draw_ellipticity_second_order_given_n(h_fig, n_max, l_max, nn, dk2_nlm, sphere_type)
% ---------------------------------------------------------------------------------------
% function to draw the second order ellipticity coefficients in rigid shells
% Here for a given radial mode number n, using scaling factor
% 190228 HCN
%
coul = 'kbrmgkbrmgkbrmgkbrmgkbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vsphoox+vsphox+vspho';
hold on
nnn = nn+1;
%
title1 = '{_n}dk2_l^m second order ellipticity coefficients x (l+1)/(m+1)/(n+1)^2';
title2 = ['for modes _nS_l in a sphere of fluid for n = ' int2str(nn)];
title3 = ['sphere type : ' sphere_type '     (' date ')'];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['second order ellipticity coefficient x (l+1)/(m+1)/(n+1){^2}'])
box on
grid on
hold on
%
for mm=0:l_max
    mmm = mm+1;
    clear to_plot compens
    to_plot(1:l_max+1-mmm+1) = dk2_nlm(nnn,mmm:l_max+1,mmm); % given n
    compens = (mmm:l_max+1)/mmm/nnn^2; % (l+1)/(m+1)/(n+1)^2
% multiply by (l+1)/(m+1)/(n+1)^2
	plot((mmm:l_max+1)-1,to_plot.*compens,[coul(mmm) marker(mmm)],'MarkerFaceColor',coul(mmm),'DisplayName',['m=' int2str(mm)])
end
legend('show')