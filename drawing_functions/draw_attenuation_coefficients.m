% ---------------------------------------------------------------------
function draw_attenuation_coefficients(h_fig, n_max,l_max,g_nl,sphere_type)
% ---------------------------------------------------------------------
% 190118 HCN change figure handling
% function to draw the attenuation half-width g_nl coefficients
% in rigid shells ()
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
title1 = 'g_{nl} half-widths for modes _nS_l';
title2 = 'in a spherical shell of fluid enclosed between inner and outer shells';
title3 = ['sphere type : ' sphere_type '     (' date ')'];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['g_{nl} half-width (Hz)'])
box on
grid on
%
for nn=0:n_max
	nnn=nn+1;
	plot((1:l_max+1)-1,g_nl(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn),'DisplayName',['n=' int2str(nn)])
end
legend('location','best')