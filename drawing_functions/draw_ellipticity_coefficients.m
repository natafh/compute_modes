% ---------------------------------------------------------------------
function draw_ellipticity_coefficients(h_fig, n_max, l_max, gamma_nl, sphere_type)
% ---------------------------------------------------------------------
% function to draw the ellipticity coefficients in elastic or rigid shells
% 190118 HCN change figure handling
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
title1 = '\gamma_{nl} ellipticity coefficients for modes _nS_l';
title2 = 'in a spherical shell of fluid enclosed between inner and outer shells';
title3 = ['sphere type : ' sphere_type '     (' date ')'];
%title2 = ['r_i=' num2str(rf(1)) ', lambda_i=' num2str(lambda(1)) ', kappa_0_i=' num2str(kappa_0(1)) ', nu_i=' num2str(nu(1))];
%title3 = ['r_o=' num2str(rf(2)) ' lambda_o=' num2str(lambda(2)) ', kappa_0_o=' num2str(kappa_0(2)) ', nu_o=' num2str(nu(2))];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['ellipticity coefficient'])
box on
grid on
for nn=0:n_max
	nnn=nn+1;
%if (superpose == 0)
	plot((1:l_max+1)-1,gamma_nl(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn),'DisplayName',['n=' int2str(nn)])
%else
%	plot((1:l_max+1)-1,ff(nnn,1:l_max+1),[coul(nnn) marker(nnn)])
%end
end
legend('location','best')