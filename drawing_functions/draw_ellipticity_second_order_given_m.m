% ---------------------------------------------------------------------------------------
function draw_ellipticity_second_order_given_m(h_fig, n_max, l_max, mm, dk2_nlm, sphere_type)
% ---------------------------------------------------------------------------------------
% function to draw the second order ellipticity coefficients in rigid shells
% Here for a given azimuthal mode number m, using scaling factor
% 190227 HCN
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
hold on
mmm = mm+1;
%
title1 = '{_n}dk2_l^m second order ellipticity coefficients x (l+1)/(m+1)/(n+1)^2';
title2 = ['for modes _nS_l in a sphere of fluid for m = ' int2str(mm)];
title3 = ['sphere type : ' sphere_type '     (' date ')'];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['second order ellipticity coefficient x (l+1)/(m+1)/(n+1){^2}'])
box on
grid on
hold on
%
compens = (mmm:l_max+1); % l+1 (starting at l=m)
for nn=0:n_max
	nnn=nn+1;
    to_plot(1:l_max+1-mmm+1) = dk2_nlm(nnn,mmm:l_max+1,mmm); % given m
% multiply by (l+1)/(n+1)^2
	plot((mmm:l_max+1)-1,to_plot.*compens/mmm/nnn^2,[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn),'DisplayName',['n=' int2str(nn)])
end
legend('show')