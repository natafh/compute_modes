% ---------------------------------------------------------------------
function draw_ellipticity_coefficients_compens(h_fig, n_max, l_max, gamma_nl, sphere_type)
% ---------------------------------------------------------------------
% function to draw the ellipticity coefficients in elastic or rigid shells
% variante pour compenser d'un facteur (l+1)^2
% 190118 HCN change figure handling
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
hold on
%
title1 = '\gamma_{nl} ellipticity coefficients x (l+1)^2 for modes _nS_l';
title2 = 'in a spherical shell of fluid enclosed between inner and outer shells';
title3 = ['sphere type : ' sphere_type '     (' date ')'];
title([title1 char(10) title2 char(10) title3]);
xlabel('mode angular number l')
ylabel(['ellipticity coefficient x (l+1)^2'])
box on
grid on
hold on
% multiply by (l+1)^2 to see the maximum m^2 splitting
compens = (1:l_max+1); % l+1
compens = compens.^2; % (l+1)^2
for nn=0:n_max
	nnn=nn+1;
	plot((1:l_max+1)-1,gamma_nl(nnn,1:l_max+1).*compens,[coul(nnn) marker(nnn)],'MarkerFaceColor',coul(nnn),'DisplayName',['n=' int2str(nn)])
end
legend('show')