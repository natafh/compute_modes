% Program to draw the combined effects of ellipticity and global rotation
% on the splitting of modes in ZoRo^2, from data in modes file
% NB : source and receiver latitudes taken into account, but not longitude
%
% 190122 HCN : correct sign error for ellipticity (was compensated by taking the opposite of Dahlen's ellipticity)
% 190116 HCN : correct bug of missing abs for source/receiver functions
% 170116 HCN : add use of computed g_nl
% 170927 HCN
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
%
marker = 'ox+vsphox+vsphox+vsphox+vsph';
couleur = 'kbrmgkbrmgkbrmgkbrmgkbrmgkbrmg';
%
fprintf(['Welcome in draw_elliptic_rotation_spectra !' char(10)])
filename = input(['enter the name of the modes file you want to plot (ex: ZoRo2-air-rigid)' char(10)],'s');
%
f_min = 500; % (Hz) minimum mode frequency
f_max = 5500; % (Hz) maximum mode frequency
%
% read modes file and associated data
[parameters,n_max,l_max,kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, ReadMe_elastic_modes] = read_acoustic_modes(filename);
l_tot = l_max+1;
n_tot = n_max+1;
% builds name of modes
for nn=0:n_max
	for ll=0:l_max
		name_ = ['_{' int2str(nn) '}S_{' int2str(ll) '}']; % ex: _0S_4
		name{nn+1,ll+1} = name_; % array of cells
	end
end
%
if isfield(parameters,'if_Mehl')
    if parameters.if_Mehl
        disp(['using Mehl ellipticity'])
    else
        disp(['using Dahlen ellipticity'])
    end
end
%
if isfield(parameters,'if_second_order')
    if parameters.if_second_order
        if_test = input(['Use second-order ellipticity correction ? (y/n) '],'s');
        if (if_test(1)=='y' || if_test(1) == 'Y') % use Mehl second order ellipticity coefficients
            use_second_order = true;
        else
            use_second_order = false;
        end
    end
end
%
if isfield(parameters,'ellipticity')
    ellipticity = parameters.ellipticity; % ellipticity
    disp(['ellipticity = ' num2str(ellipticity)])
else
    ellipticity = input(['enter the relative ellipticity (ex: 0.0517) ']);
end
Omega = input(['enter the global rotation rate in Hz (ex: 25) ']);
lat_HP = input(['enter the latitude of the loudspeaker in degrees (ex: 45) ']);
lat_micro = input(['enter the latitude of the microphone in degrees (ex: 32) ']);
    temperature = input(['enter the air temperature in degrees (ex: 20) ']);
if (0==0)
    [csound, rho_f, nu_f, kappa_f, Gruneisen] = gas_properties('air', temperature, 1e5);
    disp(['csound = ' num2str(csound) ' m/s'])
    ff = kk*csound/(2*pi*parameters.rf(2)); % frequency of modes (nn,ll)
end
% Rossby = 0.01; % typical Rossby number of the convective jets (guess...)
%
figure('Name','ellipticity and global rotation mode splitting','position',[100 100 1600 1000])
%	stem(ff(ntn,1:l_max), ((1:l_max)*0+3-ntn/5)*1000,[marker(ntn) coul(ntn)],'LineStyle',':');
%
% -----------------------------------------------------------------------
% original degenerate SNRI modes (with their name)
% -----------------------------------------------------------------------
subplot(3,1,1)
y_step = 2;
y_offset = 5; % for y-axis tick labels
%
for nn=0:n_max
	nnn=nn+1;
	hold on
	stem(ff(nnn,1:l_tot),ones(1,l_tot).*y_offset+nn,[marker(nnn) 'k'],'DisplayName',['n=' int2str(nn)]);
	text(ff(nnn,1:l_tot),ones(1,l_tot).*y_offset+1+nn,{name{nnn,1:l_tot}}, 'HorizontalAlignment', 'center') % note syntax for multiple text
end
%
xlabel('mode frequency (Hz)')
%ylabel('fake amplitude')
yticks([y_offset y_offset+y_step y_offset+2*y_step y_offset+3*y_step])
yticklabels({'0',int2str(y_step),int2str(2*y_step),int2str(3*y_step)})
ylabel('radial mode number n')
%legend('location','best')
xlim([f_min f_max])
title(['degenerate SNRI multiplets for ' filename ' at air temperature = ' num2str(temperature) ' deg'])
box on
%
% -----------------------------------------------------------------------
% multiplets with splitting due to ellipticity
% -----------------------------------------------------------------------
if (0==1)
	subplot(4,1,2)
	for nn=0:n_max
		nnn=nn+1;
		for ll=0:l_max
			lll=ll+1;
			for mm=0:ll
				mmm=mm+1;
				yy = 10.-mm*0.5;
				xx = ff(nnn,lll) * (1 + ellipticity * (-ll*(ll+1)/3. + mm^2) * gamma_nl(nnn,lll));
				if (max(xx)<f_max && min(xx)>f_min)
					stem(xx,yy,[marker(nnn) couleur(mmm)])
				end
				hold on
			end
		end
	end
	xlabel('mode frequency (Hz)')
	ylabel('fake amplitude')
	xlim([f_min f_max])
	title(['ellipticity-split singlets for \epsilon = ' num2str(ellipticity)])
	box on
	% small color (and height) legend of the m (to the right of f max)
	truc=xlim;
	for mm=0:l_max
		mmm=mm+1;
		text(truc(2)+10,10-mm*0.5,['m=' int2str(mm)],'Color',couleur(mmm))
	end
end
%
% -----------------------------------------------------------------------
% multiplets with splitting due to ellipticity + splitting due to global rotation
% -----------------------------------------------------------------------
subplot(3,1,2)
y_step = 10; % for y-axis tick labels
y_offset = 2*y_step;
%
for nn=0:n_max
	nnn=nn+1;
	for ll=0:l_max
		lll=ll+1;
		for mm=0:ll
			mmm=mm+1;
			yy(1:2) = y_offset - mm;
			if use_second_order
    			dk_nlm = Mehl_apply_second_order(nn, ll, mm, kk, ellipticity, gamma_nl, dk2_nlm);
    			xx0 = ff(nnn,lll) * (1 + dk_nlm);
			else
    			xx0 = ff(nnn,lll) * (1 + ellipticity * (-ll*(ll+1)/3. + mm^2) * gamma_nl(nnn,lll));
            end
			xx(1) = xx0 - mm*C_nl(nnn,lll)*Omega;
			xx(2) = xx0 + mm*C_nl(nnn,lll)*Omega;
			if (max(xx)<f_max && min(xx)>f_min)
				stem(xx,yy,[marker(nnn) couleur(mmm)])
			end
			hold on
		end
	end
end
xlabel('mode frequency (Hz)')
yticks([0 y_step 2*y_step])
yticklabels({int2str(2*y_step),int2str(y_step),'0'})
ylabel('azimuthal mode number \pmm')
xlim([f_min f_max])
title(['ellipticity = ' num2str(ellipticity) ' and global rotation-split singlets for f_o = ' num2str(Omega) ' Hz'])
box on
%
% -----------------------------------------------------------------------
% synthetic spectra with splitting due to ellipticity + global rotation
% -----------------------------------------------------------------------
% with Lorentzian resonance (Moldover, 1986; Trusler, 1991) and frequency
% correction due to attenuation.
% source-receiver positions taken into account via
% 	term P_l^m(cos theta_s) P_l^m(cos theta_r)
%	R^2(r_o) assuming both at the surface r=r_o
%
df = 0.1; % (Hz) frequency step
% a_N = 1; % (Pa Hz) amplitude of what ? *** test for ZoRo2
a_0 = 1.7e-3; % (Pa Hz) amplitude of what ? *** test for ZoRo2
freq = (f_min:df:f_max); % (Hz) frequency points
n_freq = length(freq);
spectrum = zeros(1,n_freq)+1e-8; % initialize spectrum
%
ctheta_s = cos((90-lat_HP)*pi/180); % cos colatitude of loudspeaker (source)
ctheta_r = cos((90-lat_micro)*pi/180); % cos colatitude of microphone (receiver)
%
for nn=0:n_max
	nnn=nn+1;
	for ll=0:l_max
		lll=ll+1;
		a_s(1:lll) = legendre(ll,ctheta_s,'norm'); % m=(0:l) P_l^m legendre polynomials at lat_HP
		a_r(1:lll) = legendre(ll,ctheta_r,'norm'); % m=(0:l) P_l^m legendre polynomials at lat_micro
% non-angular part of the amplitude from R(r) at the surface (normalized radius=1)
		a_N_0 = a_0*(spherical_bessel_j(ll,kk(nnn,lll)) + BB(nnn,lll) * spherical_bessel_y(ll,kk(nnn,lll))).^2;
		for mm=0:ll
			mmm=mm+1;
% modified mode frequencies
			if use_second_order
    			dk_nlm = Mehl_apply_second_order(nn, ll, mm, kk, ellipticity, gamma_nl, dk2_nlm);
    			xx0 = ff(nnn,lll) * (1 + dk_nlm) + delta_f_nl(nnn,lll);
			else
    			xx0 = ff(nnn,lll) * (1 + ellipticity * (-ll*(ll+1)/3. + mm^2) * gamma_nl(nnn,lll)) + delta_f_nl(nnn,lll);
            end
			xx(1) = xx0 - mm*C_nl(nnn,lll)*Omega;
			xx(2) = xx0 + mm*C_nl(nnn,lll)*Omega;
% amplitude according to source-receiver positions
% corrected (unimportant) bug missing abs 16/01/2019)
			a_N = abs(a_N_0 * a_s(mmm) * a_r(mmm)); % amplitude with angular part
			if (max(xx)<f_max && min(xx)>f_min)
				for ii=1:min(2,mm+1) % to avoid doubling m=0
					[ideb, ifin, spec] = build_Lorentzian(a_N, g_nl(nnn,lll), xx(ii), freq);
					spectrum(ideb:ifin) = spectrum(ideb:ifin) + spec(1:ifin-ideb+1);
				end
			end
			hold on
		end
	end
end
%
subplot(3,1,3)
semilogy(freq,spectrum)
xlabel('mode frequency (Hz)')
ylabel('pressure amplitude')
xlim([f_min f_max]);
%ylim([1e-5 1e-3])
title({['Lorentzian spectra for ellipticity = ' num2str(ellipticity) ' and global rotation-split singlets for f_o = ' num2str(Omega) ' Hz']; ['HP at lat ' num2str(lat_HP) ', micro at lat ' num2str(lat_micro)]})
box on
