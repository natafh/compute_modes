% example script to plot an eigenmode amplitude and kernel
% of a spherical shell
% inner radius = a
% outer radius = 1
%
% 190405 HCN
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
%
fprintf(['Welcome in plot_mode_structure !' char(10)])
fprintf(['Plots mode eigenfunction and rotational kernel' char(10) char(10)])
%
filename = input(['enter mode filename (ex: ZoRo2-air-rigid) '],'s');
[parameters,n_max,l_max,kk,ff,BB] = read_acoustic_modes(filename);
r_i = parameters.rf(1); % radius of the inner sphere (if any)
r_o = parameters.rf(2); % radius of the outer sphere

% set up the grid
r=linspace(r_i/r_o,1,101);
rayon = r';
theta=linspace(0,pi,181);
x = rayon*sin(theta);
y = rayon*cos(theta);

% select desired mode
nn = input(['   enter radial mode number n (0 to ' int2str(n_max) ') ']);
ll = input(['   enter mode degree l (0 to ' int2str(l_max) ') ']);
mm = input(['   enter mode order m (0 to ' int2str(ll) ') ']);
if (abs(mm)>ll)
    disp(['*** m > l... try again...'])
    return
end
mode_name = ['{_' int2str(nn) '}S_' int2str(ll) '^' int2str(mm)];
% n=1; l=5; m=1;
%
disp(['f = '  num2str(ff(nn+1,ll+1)) ' Hz'])
%
% calculate amplitude and plot
amplitude = A_nlm(nn,ll,mm,kk,BB,rayon,theta);
cmax = max(abs(min(min(amplitude))),abs(max(max(amplitude))));
figure('Name','mode amplitude','Position', [200 300 500 500])
pcolor(x,y,amplitude); shading flat; axis equal tight off;
caxis([-cmax cmax]); colormap('jet'); colorbar
title({filename ; [mode_name ' amplitude']},'Interpreter','tex')

%calculate kernel and plot
krnl = K_nlm(nn,ll,mm,kk,BB,r_i/r_o,rayon,theta);
cmax = max(max(krnl));
figure('Name','mode kernel','Position', [600 300 500 500])
pcolor(x,y,krnl); shading flat; axis equal tight off;
caxis([0 cmax]); colormap('hot'); colorbar
ylim([0 1])
title({filename ; [mode_name ' \omega_{fluid} kernel']},'Interpreter','tex')